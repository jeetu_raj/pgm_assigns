import math;
from pprint import pprint;
from multiprocessing import Pool
import copy
import time
import json;
import numpy as np;
import random;

num_char = 10;
num_img_ids = 1000;

poten_si_0 = {};		#10*1000
poten_si_t = {};		#10*10

all_comb_mem = {};


char_map   = {'e':0,'t':1,'a':2,'o':3,'i':4,'n':5,'s':6,'h':7,'r':8,'d':9};
chars	   = ['e','t','a','o','i','n','s','h','r','d'];
char_vals  = [0,1,2,3,4,5,6,7,8,9]

skip_cons  = 5;
skip_poten = {};

#################################################################################################################################################

def hash(list): 
	return json.dumps(list);

def unhash(string): 
	return json.loads(string);

# reads the potential si_0 from given data file
# all factors must be kept in log space
def readSi0(file_name):
	global poten_si_0;
	global num_char;
	global num_img_ids;

	poten_si_0 = {};

	file = open(file_name);
	ocr_data = file.read();
	file.close();
	ocr_list = ocr_data.strip().split("\n");
	for elem in ocr_list:
		elem_list = elem.strip().split("\t");
		char_ind  = char_map[elem_list[1]];
		img_id    = int(elem_list[0]);
		val 	  = float(elem_list[2]); #math.log(float(elem_list[2]));
		poten_si_0[(char_ind,img_id)] = val;
		

# reads the potential si_t from given data file
# all factors must be kept in log space
def readSit(file_name):
	global poten_si_t;
	global num_char;
	global num_img_ids;

	poten_si_t = {};


	file = open(file_name);
	trans_data = file.read();
	file.close();
	trans_list = trans_data.strip().split("\n");
	for elem in trans_list:
		elem_list  = elem.strip().split("\t");
		char_ind0  = char_map[elem_list[0]];
		char_ind1  = char_map[elem_list[1]];
		val 	   = float(elem_list[2]);#math.log(float(elem_list[2]));
		poten_si_t[(char_ind0,char_ind1)] = val;


#################################################################################################################################################

class var:
	id = 0;
	assign  = 0;
	range_val = 0;				#assuming all are integral assignments

	def __init__(self, id, assign ,range):
		self.id 		 = id;
		self.assign 	 = assign;
		self.range     = range;


	def print_node(self):
		print "id :: "+str(self.id);
		print "assign :: "+str(self.assign);
		print "range :: "+str(self.range);

class poten:
	id = 0;
	val = {};
	domain = [];

	def __init__(self, id, val ,domain):
		self.id 		 = id;
		self.val 	 = val;
		self.domain     = domain;


	def print_node(self):
		print "id :: "+str(self.id);
		print "val :: "+str(self.val);
		print "domain :: "+str(self.domain);


#################################################################################################################################################
prob_assign = {};

def configurePoten(observed1,observed2, case):
	potentials   = [];
	actual_poten = [];
	if(case=="ocr" or case=="ocrt" or case=="ocrts" or case=="ocrtsps"):   #check
		for a in range(0,len(observed1)):
			poten = {};
			for i in range(0,num_char):
				# print hash([i])
				poten[(i,)] = poten_si_0[ (i,observed1[a])];
			actual_poten += [poten];
			potentials   += [[a]]; 
				
		for b in range(0,len(observed2)):
			temp = b+len(observed1);
			poten = {};
			for i in range(0,num_char):
				poten[(i,)] = poten_si_0[ (i,observed2[b]) ];
			actual_poten += [poten];
			potentials   += [[temp]]; 

	if(case=="ocrt" or case=="ocrts" or case=="ocrtsps"):				#check
		for a in range(0,len(observed1)-1):
			actual_poten += [poten_si_t];
			potentials += [[a,a+1]];


		for b in range(0,len(observed2)-1):
			t = len(observed1) + b;
			actual_poten += [poten_si_t];
			potentials += [[t,t+1]];

	if(case=="ocrts" or case=="ocrtsps"):
		for a in range(0,len(observed1)):
			for b in range(a+1,len(observed1)):
				if(observed1[a] == observed1[b]):
					actual_poten += [skip_poten];
					potentials += [[a,b]];

		for a in range(0,len(observed2)):
			t = a+len(observed1);
			for b in range(a+1,len(observed2)):
				t2 = b+len(observed1);
				if(observed2[a] == observed2[b]):
					actual_poten += [skip_poten];
					potentials += [[t,t2]];

	if(case=="ocrtsps"):
		for a in range(0,len(observed1)):
			for b in range(0,len(observed2)):
				t = len(observed1)+b;
				if(observed1[a] == observed2[b]):
					actual_poten += [skip_poten];
					potentials += [[a,t]];



	return (potentials,actual_poten);

def calculatePermute(size):
	global all_comb_mem;
	try:
		return all_comb_mem[size]
	except:
		# print "not found "+str(size);
		if size == 1:
			result = [];
			for i in range(0,len(char_vals)):
				result+=[[char_vals[i]]]
			return result;
		curr = [];
		for c in char_vals:
			possible = calculatePermute(size-1);
			for each in possible:
				curr+=[each+[c]];
		all_comb_mem[size] = curr;
		return curr;


def randomSample(var_array):	#just for initialisation
	sample = [];
	for i in range(0,len(var_array)):
		x = random.randint(0,var_array[i].range-1);
		sample += [x];
		# sample += [var_array[i].range[x]];
	return sample;


def getProb(sample_array,poten_array):
	try:
		val = prob_assign[tuple(sample_array)];
		return val;
	except:
		val = 1.0;
		for each in poten_array:
			var_assign = [];
			for i in range(0,len(each.domain)):
				var_assign += [sample_array[each.domain[i]]];
			val *= each.val[tuple(var_assign)];
		prob_assign[tuple(sample_array)] = val;
		return val;


def condProbSamp(var_array,sample_array,poten_array,id_cond):
	var_cond = var_array[id_cond];
	new_prob = {};
	for i in range(0,var_cond.range):
		# var_array[id_cond].assign = i;
		sample_array[id_cond] = i;
		new_prob[i] = getProb(sample_array,poten_array);

	Z_cond = sum(new_prob.values());
	pick = random.random();
	keys = new_prob.keys();
	curr = 0;
	for i in range(0,len(keys)):
		prob = new_prob[keys[i]]/Z_cond;
		curr += prob;
		if(pick<=curr):
			return keys[i];
	# return random.randint(0,9)
	return -1;


def gibbsSample(var_array,poten_array):
	global prob_assign;
	prob_assign = {};
	t = 0;
	sample_array = randomSample(var_array);
	burn_in_over = False;
	n = len(var_array);
	T = n*500;
	R = n*50;
	sample_set = [];
	for i in range(len(sample_array)):
		temp = {};
		sample_set += [temp];


	while(not(burn_in_over) or n*t<T):
		t+=1;
		# print t;
		for id in range(0,n):
			sample = condProbSamp(var_array,sample_array,poten_array,id);
			sample_array[id] = sample;
			if(burn_in_over):
				try:
					sample_set[id][sample] += 1;
				except:
					sample_set[id][sample] = 1;
		if(not(burn_in_over)):
			if(n*t>R):
				burn_in_over = True;
				t = 1;

	# pprint(sample_set);
	# exit(0)
	max_val = 0;
	max_assign = tuple([]);
	sum_val = 0;
	for each in sample_set.keys():
		sum_val += sample_set[each];
		if(sample_set[each]>1000):
			print str(each)+" "+str(sample_set[each])
		if(sample_set[each]>max_val):
			max_assign = each;
			max_val = sample_set[each];

	return max_assign,max_val/float(sum_val);



#################################################################################################################################################

def predictGibbs(observed1,observed2):
	 (potentials,actual_poten) = configurePoten(observed1,observed2,"ocrtsps");
	 # pprint(potentials)
	 var_array = [];
	 for i in range(0,len(observed1)):
	 	temp = var(i,0,num_char);
	 	var_array += [temp];
	 for i in range(0,len(observed2)):
	 	temp = var(i+len(observed1),0,num_char);
	 	var_array += [temp];

	 poten_array = [];
	 for i in range(0,len(potentials)):
	 	# print(potentials[i])
	 	temp = poten(i,actual_poten[i],potentials[i]);
	 	poten_array += [temp]


	 val,prob = gibbsSample(var_array,poten_array);

	 word1 = ""; 
	 word2 = "";
	 for i in range(0,len(observed1)): 
	 	word1 += chars[val[i]];
	 for i in range(0,len(observed2)): 
	 	word2 += chars[val[i+len(observed1)]];

	 return word1,word2,prob;


def predict_words(image_file,word_file):
	# print "correct the file reading for two words"
	file_img = open(image_file);
	data_img = file_img.read();
	file_img.close();
	file_word = open(word_file);
	data_word = file_word.read();
	file_word.close();

	img_list = data_img.strip().split("\n\n");
	word_list = data_word.strip().split("\n\n");

	
	total_char   = 0;
	correct_char = 0;
	total_word   = 0;
	correct_word = 0;
	loglikelihood= 0;

	for id in range(0,len(img_list)):
		observed_list = img_list[id].strip().split("\n");
		observed1 = map(int,observed_list[0].strip().split("\t"));
		observed2 = map(int,observed_list[1].strip().split("\t"));

		w_list = word_list[id].strip().split("\n");
		word1 = w_list[0];
		word2 = w_list[1];

		predict_word1,predict_word2,prob = predictGibbs(observed1,observed2);
		print id
		# loglikelihood+= CalcProb(observed1,observed2,word1,word2,"ocrtsps");
		# print word1+" "+predict_word1
		# print word2+" "+predict_word2
		# exit(0)
		total_word += 1;
		if(predict_word1==word1):
			correct_word += 1;
		for ind in range(0,len(predict_word1)):
			total_char+=1;
			if(word1[ind]==predict_word1[ind]):
				correct_char+=1;

		total_word += 1;
		if(predict_word2==word2):
			correct_word += 1;
		for ind in range(0,len(predict_word2)):
			total_char+=1;
			if(word2[ind]==predict_word2[ind]):
				correct_char+=1;

	print "word wise accuracy : "+str(float(correct_word)/float(total_word));
	print "character wise accuracy : "+str(float(correct_char)/float(total_char));
	print "loglikelihood : "+str(loglikelihood);
	print "Average loglikelihood : "+str(loglikelihood/float(total_word));



if __name__ == '__main__':
	all_comb_mem[0] = [];
	readSi0("OCRdataset-2/potentials/ocr.dat");
	readSit("OCRdataset-2/potentials/trans.dat");
	for a in range(0,num_char):
		for b in range(0,num_char):
			if(a==b):
				skip_poten[(a,b)] = 5;
			else:
				skip_poten[(a,b)] = 1;


	start =  time.time();
	# print predictGibbs([582,969,582,969], [582,969,582,969]);
	# print predictGibbs([597,992,188,669,570,525], [597,623,295,570,286,221]);


	predict_words("OCRdataset-2/data/data-tree.dat","OCRdataset-2/data/truth-tree.dat")
	# print str(time.time()-start);
	# start =  time.time();
	# predict_words("OCRdataset-2/data/data-treeWS.dat","OCRdataset-2/data/truth-treeWS.dat")
	# print str(time.time()-start);
	# start =  time.time();
	# predict_words("OCRdataset-2/data/data-loops.dat","OCRdataset-2/data/truth-loops.dat")
	# print str(time.time()-start);
	# start =  time.time();
	# predict_words("OCRdataset-2/data/data-loopsWS.dat","OCRdataset-2/data/truth-loopsWS.dat")

	print str(time.time()-start);




# *gibbs sampler and inference and learning--

# - randomSampler - require the variables and range of values -- assign samples:
# - CPTS are present
# - calculate conditional probability on demand given some variables -- conditional is chill as Z not required
# - sample the next state
# - loop

# *conditional probability calculation--

# - all potentials present,  assignment provided and calculation performed -- and samples collected
# most probable assignment is the one that occurs most popularly


# Next:

# parameter learning:

# - Bayesian network :
# - simple calculation of fractions from the data - 
# - then infer from data

# - markov network :
# - gradient descent to get the cpts