#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <cstdio>
#include <fstream>
#include <time.h>  
#include <string.h>   
#include <algorithm> 
#include <queue>
#include <stdio.h>
#include <stack>
//may need to remove this as slightly complex data structure
#include <map>
using namespace std;

/*
* convention : 
* class start with capital
* functions camelcase
* variables _
*/

/*
* linked list implementation
*/
class BayesNet
{
	public:
		vector<vector<int> > children;
		vector<vector<int> > parents;
		int n_nodes;
	private:
		vector<int> observed;
		vector<int> ancestor_obs;


	public:
		BayesNet();

		BayesNet(string input_file);

		void createRandomGraph(int nodes,int k);

		void saveBN(string output_file);

		void printBN();

		void initObserved(vector<int> observed);

		string checkDseparation(int X,int Y);

		void handleQuery(string query_file);

};


/*
* simple split in C++
*/
vector<string> split(const string &s, char delim) 
{
    stringstream ss(s);
    string item;
    vector<string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}

/*
* simple constructor
*/
BayesNet::BayesNet()
{
	n_nodes = 0;
	// for(int i = 0 ; i < n_nodes+1 ; i++)
	// {
	// 	observed.push_back(0);
	// 	ancestor_obs.push_back(0);
	// }
	// for(int i = 0 ; i < n_nodes ; i++)
	// {
	// 	vector<int> a;
	// 	vector<int> b;
	// 	parents.push_back(a);
	// 	children.push_back(b);
	// }
}

/*
* INPUT : input_file name
* OUPUT : none
* WORK  : reads bayesian network from file and initialises the network
*/
BayesNet::BayesNet(string input_file)
{
	ifstream fin;
	fin.open(input_file);	
	fin>>n_nodes;
	for(int i = 0 ; i < n_nodes ; i++)
	{
		vector<int> a;
		vector<int> b;
		parents.push_back(a);
		children.push_back(b);
	}

	for(int i = 0 ; i < n_nodes ; i++)
	{
		int curr_node;
		fin>>curr_node;
		string nghbrs;
		fin>>nghbrs;
		// cout<<nghbrs<<endl;
		nghbrs = nghbrs.substr(0,nghbrs.size()-1);
		nghbrs = nghbrs.substr(1,nghbrs.size());
		
		vector<string> ngbr_list = split(nghbrs,',');
		for(int j = 0 ; j < ngbr_list.size() ; j++ )
		{
			int x = stoi(ngbr_list[j]);
			children[curr_node].push_back(x);
			parents[x].push_back(curr_node);
		}

	}
	fin.close();

	for(int i = 0 ; i < n_nodes+1 ; i++)
	{
		observed.push_back(0);
		ancestor_obs.push_back(0);
	}
}

/*
* INPUT : no. of nodes
* OUPUT : none
* WORK  : initialises the network with a random dag representing a bayesian network
*/
void BayesNet::createRandomGraph(int nodes,int k)
{
	// cout<<nodes<<" "<<k<<endl;
	if(parents.size()>0)
	{
		parents.erase(parents.begin(),parents.end());
		children.erase(children.begin(),children.end());
		n_nodes = 0;
	}
	n_nodes = nodes;
	for(int i = 0 ; i < n_nodes+1 ; i++)
	{
		observed.push_back(0);
		ancestor_obs.push_back(0);
	}

	for(int i = 0 ; i < n_nodes+1 ; i++)
	{
		vector<int> a;
		vector<int> b;
		parents.push_back(a);
		children.push_back(b);
	}

	for(int i = 1 ; i < n_nodes+1 ; i++)
	{

		int child_count = (rand()%(k+1));
		if(n_nodes-i<child_count)
		{
			for(int j = i+1 ; j < n_nodes+1 ; j++)
			{
				// printf("i:%d j:%d\n",i,j );
				children[i].push_back(j);
				parents[j].push_back(i);
			}
		}
		else
		{
			vector<int> temp_nums;
			for(int j = i+1 ; j < n_nodes+1 ; j++)
				temp_nums.push_back(j);
			random_shuffle(temp_nums.begin(),temp_nums.end());
			// cout<<"here x 4 i:"<<i<<" child_count :"<<child_count<<endl;
			// printf("here x 4 i:%d child_count:%d temp_size:%d\n",i,child_count,temp_nums.size() );
			for(int p = 0 ; p < child_count ; p++)
			{
				children[i].push_back(temp_nums[p]);
				parents[temp_nums[p]].push_back(i);
			}

		}

	}

}

/*
* INPUT : output_file name
* OUPUT : none
* WORK  : saves bayesian network in a file
*/
void BayesNet::saveBN(string output_file)
{
	ofstream fout;
	fout.open(output_file);
	fout<<n_nodes<<endl;

	for(int i = 1 ; i < n_nodes+1 ; i++)
	{
		fout<<i<<" ";
		fout<<"[";
		for(int j = 0 ; j < children[i].size() ; j++ )
		{
			fout<<children[i][j];
			if(j<children[i].size()-1)
				fout<<",";
		}
		fout<<"]"<<endl;

	}

	fout.close();
}

/*
* INPUT : 
* OUPUT : none
* WORK  : prints bayesian network to commandline
*/
void BayesNet::printBN()
{
	cout<<n_nodes<<endl;

	for(int i = 1 ; i < n_nodes+1 ; i++)
	{
		cout<<i<<" ";
		cout<<"[";
		for(int j = 0 ; j < children[i].size() ; j++ )
		{
			cout<<children[i][j];
			if(j<children[i].size()-1)
				cout<<",";
		}
		cout<<"]"<<endl;

	}

}


/*
* INPUT : vector of observed nodes
* OUPUT : none
* WORK  : sets up phase 1
*/
void BayesNet::initObserved(vector<int> Z)
{

	for(int i = 0 ; i < n_nodes+1 ; i++)
	{
		observed[i] = 0;
		ancestor_obs[i] = 0;
	}

	
	//phase 1 - marking all the ancestors of the observed inorder to deal with V structures 
	for(int i = 0 ; i < Z.size() ; i++)
	{
		// cout<<Z[i]<<endl;
		this->observed[Z[i]] = 1;
		int curr_node = Z[i];

		queue<int> ances;
		ances.push(curr_node);
		while(ances.size()>0)
		{
			int top = ances.front();
			ances.pop();
			this->ancestor_obs[top] = 1;
			for(int j = 0 ; j < parents[top].size() ; j++)
			{
				ances.push(parents[top][j]);
			}
		}
	}
}

/*
* INPUT : two nodes X,Y
* OUPUT : suitable output to show path if active trail exists otherwise yes
* WORK  : traverses in a dfs fashion checking paths and storing it at any moment in curr_path to show active trail
* dfs or bfs is suitable as if one path is blocked then any other path having common with it is also blocked 
*/
string BayesNet::checkDseparation(int X,int Y)
{
	int *up_arrow = new int[n_nodes+1];
	int *down_arrow = new int[n_nodes+1];
	memset(up_arrow,0,sizeof(int)*(n_nodes+1));
	memset(down_arrow,0,sizeof(int)*(n_nodes+1));

	bool is_dependent = false;
	pair<int,int > dest;

	queue< pair<int,int> > bfs;						//replacing by stack and searching in a depth first way
	bfs.push(make_pair(X,1));						//1 for up 0 for down

	map<pair<int,int>,pair<int,int>> mapping;

	mapping[make_pair(X,1)] = make_pair(-1,-1);	//parent of root marked as null

	while(bfs.size() > 0)
	{
		pair<int,int > curr = bfs.front();
		bfs.pop();
		int node = curr.first;
		int arrow_flag = curr.second;
		if(node==Y)
		{
			is_dependent = true;
			dest = curr;
			break;
		}

		if(arrow_flag==1)
		{	
				// cout<<node<<" node "<<arrow_flag<<endl;

			if(up_arrow[node]==0 && this->observed[node]==0)
			{

				up_arrow[node]=1;					//marking as visited
				for(int i = 0 ; i < parents[node].size() ; i++)
				{
					bfs.push(make_pair(parents[node][i],1));
					if(mapping.count(make_pair(parents[node][i],1)) < 1)
						mapping[make_pair(parents[node][i],1)] = curr;
					// cout<<parents[node][i]<<" ";
				}
				// cout<<" par"<<endl;
				for(int i = 0 ; i < children[node].size() ; i++)
				{
					bfs.push(make_pair(children[node][i],0));
					if(mapping.count(make_pair(children[node][i],0)) < 1)
						mapping[make_pair(children[node][i],0)] = curr;
					// cout<<children[node][i]<<" ";

				}
				// cout<<" child"<<endl;

			}
			else if(up_arrow[node]==0)
				up_arrow[node]=1;
		}
		else
		{
				// cout<<node<<" node "<<arrow_flag<<endl;

			if(down_arrow[node]==0 && this->observed[node]==0)
			{

				down_arrow[node]=1;					//marking as visited
				for(int i = 0 ; i < children[node].size() ; i++)
				{
					// cout<<children[node][i]<<" ";
					bfs.push(make_pair(children[node][i],0));
					if(mapping.count(make_pair(children[node][i],0)) < 1)
						mapping[make_pair(children[node][i],0)] = curr;
				}
				// cout<<" par"<<endl;


			}
			else if(down_arrow[node]==0 && this->ancestor_obs[node]==1)
			{
					// cout<<node<<" "<<arrow_flag<<" "<<mapping[curr].first<<" "<<mapping[curr].second<<endl;

					down_arrow[node] = 1;
					for(int i = 0 ; i < parents[node].size() ; i++)
					{
						// cout<<parents[node][i]<<" ";
						bfs.push(make_pair(parents[node][i],1));
						if(mapping.count(make_pair(parents[node][i],1)) < 1)
							mapping[make_pair(parents[node][i],1)] = curr;
					}
				// cout<<" par"<<endl;

			}
			else
			{
				down_arrow[node] = 1;
			}
		}
	}
	// cout<<"its done"<<endl;
	delete[] up_arrow;
	delete[] down_arrow;

	if(is_dependent)
	{
		map<int,int> temp_dict;
		vector<int> nums;
		while(dest.first>0)
		{
			
			nums.push_back(dest.first);
			try
			{
				temp_dict[dest.first]++;
			}
			catch(int e)
			{
				temp_dict[dest.first] = 1;
			}
			dest = mapping[dest];
		}

		string output = "]";
		for(int i = 0; i < nums.size() ; i++)
		{
			if(temp_dict[nums[i]]==1)
			{
				if(output.size()>1)
					output = to_string(nums[i])+","+output;
				else
					output = to_string(nums[i])+output;
			}
			else
			{
				int temp = i;

				i++;
				while(nums[temp]!=nums[i])
					i++;
				temp_dict[i] = 1;

				if(output.size()>1)
					output = to_string(nums[i])+","+output;
				else
					output = to_string(nums[i])+output;

			}

		}
		output = "no ["+output;
		return output;
	}
	else
	{
		return "yes";
	}

}

/*
* INPUT : query_file name
* OUPUT : none
* WORK  : handles all the queries and outputs suitably
*/
void BayesNet::handleQuery(string query_file)
{
	ifstream fin;
	fin.open(query_file);
	ofstream fout;
	fout.open("out.txt");
	int q;
	fin>>q;
	while(q--)
	{
		int x,y;
		string z;
		fin>>x>>y;
		fout<<x<<" "<<y<<" ";
		fin>>z;
		fout<<z<<endl;
		z = z.substr(0,z.size()-1);
		z = z.substr(1,z.size());

		vector<string> z_list = split(z,',');
		vector<int> obs_given;
		// cout<<obs_given.size()<<endl;
		for(int j = 0 ; j < z_list.size() ; j++ )
		{
			int curr = stoi(z_list[j]);
			obs_given.push_back(curr);
			// cout<<curr<<" ";
		}
		// cout<<endl;
		initObserved(obs_given);
		// for(int i = 0; i < this->observed.size() ; i++)
		// 	cout<<this->observed[i]<<" ";
		// cout<<endl;

		// for(int i = 0; i < this->ancestor_obs.size() ; i++)
		// 	cout<<this->ancestor_obs[i]<<" ";
		// cout<<endl;

		// this->printBN();
		// cout<<"-------------------"<<endl;
		string out = checkDseparation(x,y);
		// this->printBN();

		// cout<<"-------------------"<<endl;
		// cout<<out<<endl;
		fout<<out<<endl;
	}
	// cout<<endl;
	// cout<<endl;
	fin.close();
	fout.close();
}


int main( int argc , char *argv[])
{
	srand(time(0));
	if(argc<3)
	{
		cout<<"please place both arguments bn.txt query.txt"<<endl;
		exit(0);
	}
	BayesNet BN = BayesNet(argv[1]);
	BN.handleQuery(argv[2]);

}