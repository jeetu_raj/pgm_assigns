import math;
from pprint import pprint;
from multiprocessing import Pool
import copy
import time
import json;

num_char = 10;
num_img_ids = 1000;

poten_si_0 = {};		#10*1000
poten_si_t = {};		#10*10

all_comb_mem = {};


char_map   = {'e':0,'t':1,'a':2,'o':3,'i':4,'n':5,'s':6,'h':7,'r':8,'d':9};
chars	   = ['e','t','a','o','i','n','s','h','r','d'];
char_vals  = [0,1,3,4,5,6,7,8,9]


#################################################################################################################################################

def hash(list): 
	return json.dumps(list);

def unhash(string): 
	return json.loads(string);

# reads the potential si_0 from given data file
# all factors must be kept in log space
def readSi0(file_name):
	global poten_si_0;
	global num_char;
	global num_img_ids;

	poten_si_0 = {};

	file = open(file_name);
	ocr_data = file.read();
	file.close();
	ocr_list = ocr_data.strip().split("\n");
	for elem in ocr_list:
		elem_list = elem.strip().split("\t");
		char_ind  = char_map[elem_list[1]];
		img_id    = int(elem_list[0]);
		val 	  = float(elem_list[2]); #math.log(float(elem_list[2]));
		poten_si_0[hash([char_ind,img_id])] = val;
		

# reads the potential si_t from given data file
# all factors must be kept in log space
def readSit(file_name):
	global poten_si_t;
	global num_char;
	global num_img_ids;

	poten_si_t = {};


	file = open(file_name);
	trans_data = file.read();
	file.close();
	trans_list = trans_data.strip().split("\n");
	for elem in trans_list:
		elem_list  = elem.strip().split("\t");
		char_ind0  = char_map[elem_list[0]];
		char_ind1  = char_map[elem_list[1]];
		val 	   = float(elem_list[2]);#math.log(float(elem_list[2]));
		poten_si_t[hash([char_ind0,char_ind1])] = val;


#################################################################################################################################################

class node:
	id = 0;
	poten     = {};
	belief    = 1.0;
	level     = 0;
	current   = [];
	msg_flag  = {};
	msg_vars  = {};
	msgs      = {};
	ngbrs     = [];


	def __init__(self, id, ngbrs ,current):
		self.id 		 = id;
		self.current 	 = current;
		self.ngbrs     = ngbrs;
		self.msg_flag  = {};

	def print_node(self):
		print "id :: "+str(self.id);
		print "domain :: "+str(self.current);
		print "ngbrs :: "+str(self.ngbrs);
		print "msg_vars ::"+str(self.msg_vars)
		print "msg_flags ::"+str(self.msg_flag)

#################################################################################################################################################


def configurePoten(observed1,observed2, case):
	potentials   = [];
	actual_poten = [];
	adj_list     = {};
	for i in range(0,len(observed2)+len(observed1)):
		adj_list[i] = [];
	if(case=="ocr" or case=="ocrt" or case=="ocrts" or case=="ocrtsps"):
		for a in range(0,len(observed1)):
			poten = {};
			for i in range(0,num_char):
				# print hash([i])
				poten[hash([i])] = poten_si_0[ hash([i,observed1[a]])];
			actual_poten += [poten];
			potentials   += [[a]]; 
				
		for b in range(0,len(observed2)):
			temp = b+len(observed1);
			poten = {};
			for i in range(0,num_char):
				poten[hash([i])] = poten_si_0[ hash([i,observed2[b]]) ];
			actual_poten += [poten];
			potentials   += [[temp]]; 

	if(case=="ocrt" or case=="ocrts" or case=="ocrtsps"):
		for a in range(0,len(observed1)-1):
			actual_poten += [poten_si_t];
			potentials += [[a,a+1]];
			adj_list[a] += [a+1];
			adj_list[a+1] += [a];


		for b in range(0,len(observed2)-1):
			t = len(observed1) + b;
			actual_poten += [poten_si_t];
			potentials += [[t,t+1]];
			adj_list[t] += [t+1];
			adj_list[t+1] += [t];


	return (potentials,actual_poten,adj_list);

def calculatePermute(size):
	global all_comb_mem;
	try:
		return all_comb_mem[size]
	except:
		# print "not found "+str(size);
		if size == 1:
			result = [];
			for i in range(0,len(char_vals)):
				result+=[[char_vals[i]]]
			return result;
		curr = [];
		for c in char_vals:
			possible = calculatePermute(size-1);
			for each in possible:
				curr+=[each+[c]];
		all_comb_mem[size] = curr;
		return curr;

def combine( pot_ranges , act_poten , domain):
	temp_dict = {};
	for i in range(0,len(domain)):
		temp_dict[domain[i]] = i;
	all_perm = calculatePermute(len(domain))
	result_dict = {};
	# pprint(pot_ranges);
	# pprint(act_poten);
	# pprint(all_perm);

	for elem in all_perm:
		for j in range(0,len(act_poten)):
			val = [];
			for k in range(0,len(pot_ranges[j])):
				# print pot_ranges[j][k]
				# print temp_dict[pot_ranges[j][k]]
				# print elem
				val += [elem[temp_dict[pot_ranges[j][k]]]];
			try:
				result_dict[hash(elem)] *= act_poten[j][hash(val)];
			except:
				result_dict[hash(elem)]  = act_poten[j][hash(val)];
	return result_dict;

def get_ordering(adj_list):
	order = [];
	V = adj_list.keys();
	# pprint(adj_list);
	while(len(V)>0):
		max_deg = -1;
		max_v   = -1;
		for vertex in V:
			deg = 0;
			try:
				deg = len(adj_list[vertex]);
			except:
				deg = 0;
			if(deg>max_deg):
				max_deg = deg;
				max_v   = vertex;
		V.remove(max_v);
		# pprint(adj_list)
		for i in range(0,len(adj_list[max_v])):
			adj_list[adj_list[max_v][i]].remove(max_v)
			for j in range(i+1,len(adj_list[max_v])):
				adj_list[adj_list[max_v][i]] += [adj_list[max_v][j]]
				adj_list[adj_list[max_v][j]] += [adj_list[max_v][i]]
		del adj_list[max_v];
		order += [max_v];
	# print order
	return order;


#################################################################################################################################################


# this will return an array of nodes representing a clique tree based on minfill algorithm
# the input provided specifies a specific markov network with given variables, here the graph
# is visualised as adjusted 
def makeCTree(observed1, observed2, case):
	poten_list   = [];				#marks out the original potentials between elements;
	actual_poten = [];
	node_list    = [];				#nodes of graph based on si;
	msg_list     = [];				#msg passed by nodes which are actually new potentials in variable elimination but kept separate here;
	marked_pot   = [];
	adj_list 	 = {}
	(poten_list,actual_poten,adj_list) = configurePoten(observed1,observed2, case);
	ve_order = get_ordering(adj_list);

	# print poten_list;
	# pprint(actual_poten);
	# print ve_order

	marked_pot   = [0]*len(poten_list);
	#initialize all the above
	for elem in range(0,len(ve_order)):
		var = ve_order[elem];
		si  = set([]);
		temp_pot   = [];
		temp_act   = [];
		n1 = node(len(node_list),[],[]);
		for i in range(0,len(poten_list)):	#covering all the concerned potentials
			if(marked_pot[i]==1):
				continue;
			each = poten_list[i]
			if var in each:
				# print str(var)+" "+str(each)
				si = si | set(each);
				temp_pot+= [poten_list[i]];
				temp_act+= [actual_poten[i]];
				marked_pot[i] = 1;

		print msg_list
		for j in range(0,len(msg_list)):	#covering all the elements
			if var in msg_list[j]:
				node_list[j].ngbrs += [elem];
				n1.ngbrs += [j];
				si = si | set(msg_list[j]);	
				# print str(var)+" "+str(msg_list[j])
		
		n1.current = list(si);
		t_list     = list(si);
		t_list.remove(var);						 #sexit(0)ince it has been eliminated
		msg_list  += [t_list];		
		n1.poten = combine(temp_pot,temp_act,n1.current);   #of the dimension of the current of the node
		node_list +=[n1]

	for each in node_list:
		for i in each.ngbrs:
			each.msg_flag[i] = 0 ;
			each.msgs[i] = {};
	# print node_list
	for i in node_list:
		i.print_node();
	# exit(0);
	return node_list;

# this will simply return the cluster graph corresponding to original graphical models
# this should be in a bethe cluster format . One list of nodes , one list of potentials with edges on both sides
def makeClusterGraph(observed1, observed2):
	node_list = [];
	for i in range(0,len(observed1)+len(observed2)):
		n = node(i,[],[])
		node_list += [n];
	
	if(case=="ocr"):						#filling up the potential list of the 
		node_poten_list = [];
		for a in range(0,len(observed1)):
			n1 = node(a,[a],[a]);
			node_list[a].ngbrs += [a];
			node_list[a].msg_flag += [0];
			for i in range(0,num_char):
				n1.poten[i] = poten_si_0[hash([i,observed1[a]])];
			node_poten_list += [n1]; 
				
		for b in range(0,len(observed2)):
			temp = b+len(observed1);
			n2 = node(temp,[temp],[temp]);
			n2.poten = [];
			node_list[temp].ngbrs += [temp];
			node_list[a].msg_flag += [0];
			for i in range(0,num_char):
				n2.poten[i] = poten_si_0[hash([i,observed2[b]])];
			node_poten_list += [n2];
		return (node_list,node_poten_list);
				

				
#################################################################################################################################################

def initialise_root(node_list, root):
	queue = [];
	print root
	node_list[root].level = 0;
	queue += [root];
	marked = [0]*len(node_list);
	parent_ind = [-1]*len(node_list);
	print "start"
	while(len(queue)>0):
		front = queue[0];
		queue.pop(0);
		if(marked[front]==1):
			continue;
		print front
		marked[front] = 1;
		for i in range(0,len(node_list[front].ngbrs)):
			each = node_list[front].ngbrs[i];
			if(marked[each]==0):
				print "neighbr "+str(each)
				queue += [each];
				node_list[each].level = node_list[front].level+1;
			else:
				parent_ind[front] = node_list[front].ngbrs[i];		#actual parent index


	print parent_ind
	return (node_list,parent_ind);

def combine_message(n):
	elems = n.current;
	rev_map = {};
	for i in range(0,len(elems)):
		rev_map[elems[i]] = i;

	for ele in n.poten.keys():
		elem = unhash(ele);
		for i in range(0,len(msgs)):
			a = [];
			for j in range(0,len(msg_vars[i])):
				a += [rev_map[msg_vars[i][j]]];
			n.poten[elem] =  n.poten[ele]*msgs[i][hash(a)]
	return n;

def combine_msg_ind(n,par):
	elems = n.current;
	rev_map = {};
	for i in range(0,len(elems)):
		rev_map[elems[i]] = i;

	for ele in n.poten.keys():
			elem = unhash(ele);
			a = [];
			for j in range(0,len(n.msg_vars[par])):
				a += [rev_map[n.msg_vars[par][j]]];
			n.poten[elem] =  n.poten[ele]*n.msgs[par][hash(a)]
	return n;


def merge(list1 , list2 , order1 ):
	a = [-1]*(len(list1)+len(list2));
	for i in range(0,len(list1)):
		a[order1[i]] = list1[i];
	k = 0;
	for i in range(0,len(a)):
		if a[i]<0:
			a[i] = list2[k];
			k+=1;
	return a;

def make_message_ind(n, ind):
	msg = {};
	pos = [];
	for i in range(0,len(n.msg_vars[ind])):
		elem = n.msg_vars[ind][i];
		pos+=[n.current.index(elem)];

	perms  = calculatePermute(len(n.msg_vars[ind]));
	perms2 = calculatePermute(len(n.current)-len(n.msg_vars[ind]));
	for each in perms:
		msg[hash(each)] = 0.0;
		for elem in perms2:
			e = merge(each,elem,pos);
			msg[hash(each)] = msg[hash(each)]+n.poten[e]/msgs[ind][each];			#removing the old message present in it
	return msg;


def make_message(n, par):
	msg = {};
	pos = [];
	for i in range(0,len(n.msg_vars[par])):
		elem = n.msg_vars[par][i];
		pos+=[n.current.index(elem)];

	perms  = calculatePermute(len(n.msg_vars[par]));
	perms2 = calculatePermute(len(n.current)-len(n.msg_vars[par]));
	for each in perms:
		msg[hash(each)] = 0.0;
		for elem in perms2:
			e = merge(each,elem,pos);
			msg[hash(each)] = msg[hash(each)]+n.poten[e];
	return msg;

#this will return the node list with updated beliefs for each node
def CTreeSPComplete( node_list ):
	root = len(node_list)-1;
	(node_list,parent_ind) = initialise_root(node_list, root);
	ready = [0]*len(node_list);
	for i in range(0,len(node_list)):
		if(len(node_list[i].ngbrs)<= 1 and i!=root):		#this ensures that no leaf is taken when it has incoming edge
			ready[i] = 1;	#as leaves so ready

		par = parent_ind[i]
		if(par>=0):
			node_list[i].msg_flag[par] = 2;		#to mark the parent in this setting so message is not required in up pass
	
	if(len(node_list[root].ngbrs)==0):
		ready[root] = 1;

	
	#upwards section
	while(ready[root]==0):
		for i in range(0,len(ready)):
			if ready[i]==1:
				par = parent_ind[i]
				if(par<0):					#there is no parent
					ready[i] = 2;
					continue;

				node_list[i] = combine_message(node_list[i]);
				msg = make_message(node_list[i],par);

				node_list[par].msg_flag[i] = 1;
				node_list[par].msgs[i] = msg;
				check = False;
				for j in node_list[par].msg_flag.values():
					if(j==0):
						check = True;
						break;
				if(not(check)):
					ready[par] = 1;
				ready[i] = 2;

	# exit(0);
	#downwards section
	ready_rev = [0]*len(node_list);
	ready_rev[root] = 1; 

	while(0 in ready_rev):
		for i in range(0,len(ready_rev)):
			par  = parent_ind[i];
			if(par<0):					#there is no parent
				ready_rev[i] = 2;
				continue;
			if ready_rev[i] == 1:

				node_list[i] = combine_msg_ind(node_list[i],par);
				for j in range(node_list[i].ngbrs):
					curr = node_list[i].ngbrs[j];
					if(curr==par):
						continue;
					msg = make_message_ind(n,curr);
					node_list[curr].msg_flag[par] = 1;
					node_list[curr].msgs[par] = msg;
					ready_rev[par] = 1;
				ready_rev[i] = 2;

	#updated beliefs at each node
	return node_list;

def predictSP(observed1, observed2,case):
	n_list = makeCTree(observed1, observed2,case);
	n_list = CTreeSPComplete(n_list);
	predict_word_ids = [0]*(len(observed1)+len(observed2));
	marginals = {};
	for i in n_list:
		node = i
		elems = node.current;
		n_marg = [];
		n_marg_ind = {};
		for j in range(0,len(node.current)):
			elem = node.current[j]
			try:
				if( len(marginals[elem]) >= 0):
					continue;
			except:
				marginals[elem] = [0]*num_char;
				n_marg += [elem]
				n_marg_ind[elem] = j;

		for each in node.poten.keys():
			mems = unhash(each);
			for p in n_marg:
				index = n_marg_ind[p];
				char = mems[index];
				marginals[p][char] += node.poten[each];


	word1 = "";
	word2 = "";
	for i in range(0,len(observed1)):
		val = max(marginals[i]);
		char = marginals[i].index(val);
		word1 += chars[char];

	for i in range(0,len(observed2)):
		val = max(marginals[i+len(observed1)]);
		char = marginals[i+len(observed1)].index(val);
		word2 += chars[char];

	print word1
	print word2
	return (word1,word2);

		 

#this will return the node list with updated beliefs for each node
def LoopySPBP( node_list ):
	a = 2
#################################################################################################################################################

def predict_words(image_file,word_file):
	print "correct the file reading for two words"
	exit(0);
	file_img = open(image_file);
	data_img = file_img.read();
	file_img.close();
	file_word = open(word_file);
	data_word = file_word.read();
	file_word.close();
	img_list = data_img.strip().split("\n");
	word_list = data_word.strip().split("\n");

	total_char   = 0;
	correct_char = 0;
	total_word   = 0;
	correct_word = 0;
	loglikelihood= 0;

	for id in range(0,len(img_list)):
		observed = map(int,img_list[id].strip().split("\t"));

		(predict_word,likelihood) = predictSP(observed);
		
		loglikelihood+= math.log(likelihood)
		
		total_word += 1;
		if(predict_word==word_list[id]):
			correct_word += 1;
		for ind in range(0,len(predict_word)):
			total_char+=1;
			if(word_list[id][ind]==predict_word[ind]):
				correct_char+=1;


	print "word wise accuracy : "+str(float(correct_word)/float(total_word));
	print "character wise accuracy : "+str(float(correct_char)/float(total_char));
	print "loglikelihood : "+str(loglikelihood);
	print "Average loglikelihood : "+str(loglikelihood/len(word_list));


if __name__ == '__main__':
	all_comb_mem[0] = [];
	readSi0("OCRdataset-2/potentials/ocr.dat");
	readSit("OCRdataset-2/potentials/trans.dat");
	# print len(poten_si_0);
	# print len(poten_si_0[0]);
	# pprint(poten_si_0)
	# print len(poten_si_t);
	# print len(poten_si_t[0]);
	# pprint(poten_si_t)
	start =  time.time();

	# print calculateScoreOCR([582,969,582,969],"adad");
	# print calculateScoreTrans([582,969,582,969],"adad");
	# print calculateScoreComb([582,969,582,969],"adad");
	node_list = predictSP([582,969,582,969], [582,969,582,969], "ocrt");
	# print node_list
	# CTreeSPComplete(node_list);
	# predict_words("OCRdataset/data/small/images.dat","OCRdataset/data/small/words.dat")
	print time.time()-start;


#################################################################################################################################################

#approach
# 2. create a clique tree datastructure -- node contains : self potential , children , parent , msg_flag , belief , msg
# 3. implement message passing










# def makeCTree(observed1, observed2, case):
	
# 	if(case=="ocr"):
# 		ve_order = get_ordering(observed1, observed2, "ocr");
# 		node_list = [];
# 		msg_list  = [];
# 		#take each vertex in elimination and create a node si
# 		#then also track the message it would send and create edges with the current based on this
# 		for elem in range(0,len(ve_order)):
# 			#setup node
# 			n1 = node(elem,[],[elem]);
# 			m1 = [];							#message stores all except eliminated in it
# 			node_list+=[n1];
# 			msg_list +=[m1];
# 			for i in range(0,num_char):
# 				if(elem < len(observed1)):
# 					n1.poten[i] = poten_si_0[i][observed1[elem]];
# 				else:
# 					n1.poten[i] = poten_si_0[i][observed2[elem-len(observed1)]];

# 		return node_list;

# 	else if(case=="ocrt"):
# 		ve_order = get_ordering(observed1, observed2, "ocrt");
# 		poten_list   = [];				#marks out the original potentials between elements;
# 		actual_poten = [];
# 		node_list    = [];				#nodes of graph based on si;
# 		msg_list     = [];				#msg passed by nodes which are actually new potentials in variable elimination but kept separate here;
# 		marked_pot   = [];

# 		(poten_list,actual_poten) = configure_poten(observed1,observed2, "ocrt");
# 		#initialize all the above
# 		for elem in range(0,len(ve_order)):

# 			var = ve_order[elem];
# 			si  = set([]);
# 			temp_pot   = [];
# 			temp_act   = [];
# 			n1 = node(len(node_list),[],[]);
# 			for i in range(0,len(poten_list)):
# 				if(marked_pot[i]==1):
# 					continue;
# 				each = poten_list[i]
# 				if var in each:
# 					si = si | set(each);
# 					temp_pot+= [poten_list[i]];
# 					temp_act+= [actual_poten[i]];
# 					marked_pot[i] = 1;

# 			n1 = node(elem,[],[]);
# 			for j in range(0,len(msg_list)):
# 				if var in msg_list[j]:
# 					node_list[j].ngbrs += [elem];
# 					n1.ngbrs += [j];
# 					si = si | set(msg_list[j]);

# 			n1.current = list(si);
# 			t_list     = list(si);
# 			t_list.remove(var);						 #since it has been eliminated
# 			msg_list  += t_list;
# 			n1.poten = combine(temp_pot,temp_act,n1.current);   #of the dimension of the current of the node

# 		return node_list;



# def configurePoten(observed1,observed2, case):
# 	potentials = [];
# 	actual_poten = [];
# 	if(case=="ocr" or case=="ocrt" or case=="ocrts" or case=="ocrtsps"):
# 		for a in range(0,len(observed1)):
# 			poten = [];
# 			for i in range(0,num_char):
# 				poten += [poten_si_0[i][observed1[a]]];
# 			actual_poten += [poten];
# 			potentials   += [a]; 
				
# 		for b in range(0,len(observed2)):
# 			temp = b+len(observed1);
# 			poten = [];
# 			for i in range(0,num_char):
# 				poten += [poten_si_0[i][observed2[b]]];
# 			actual_poten += [poten];
# 			potentials   += [temp]; 

# 	if(case=="ocrt" or case=="ocrts" or case=="ocrtsps"):
# 		for a in range(0,len(observed1)-1):
# 			poten = [];
# 			for i in range(0,num_char):
# 				t_poten = [];
# 				for j in range(0,num_char):
# 					t_poten += [poten_si_t[i][j]]
# 				poten += [t_poten];
# 			actual_poten += [poten];
# 			potentials += [[a,a+1]]

# 		for b in range(0,len(observed2)-1):
# 			t = len(observed1) + b;
# 			poten = [];
# 			for i in range(0,num_char):
# 				t_poten = [];
# 				for j in range(0,num_char):
# 					t_poten += [poten_si_t[i][j]]
# 				poten += [t_poten];
# 			actual_poten += [poten];
# 			potentials += [[t,t+1]]