import math;
from pprint import pprint;
from multiprocessing import Pool
import copy
import time
import json;

num_char = 10;
num_img_ids = 1000;

poten_si_0 = {};		#10*1000
poten_si_t = {};		#10*10

all_comb_mem = {};


char_map   = {'e':0,'t':1,'a':2,'o':3,'i':4,'n':5,'s':6,'h':7,'r':8,'d':9};
chars	   = ['e','t','a','o','i','n','s','h','r','d'];
char_vals  = [0,1,2,3,4,5,6,7,8,9]

skip_cons  = 5;
skip_poten = {};

#################################################################################################################################################

def hash(list): 
	return json.dumps(list);

def unhash(string): 
	return json.loads(string);

# reads the potential si_0 from given data file
# all factors must be kept in log space
def readSi0(file_name):
	global poten_si_0;
	global num_char;
	global num_img_ids;

	poten_si_0 = {};

	file = open(file_name);
	ocr_data = file.read();
	file.close();
	ocr_list = ocr_data.strip().split("\n");
	for elem in ocr_list:
		elem_list = elem.strip().split("\t");
		char_ind  = char_map[elem_list[1]];
		img_id    = int(elem_list[0]);
		val 	  = float(elem_list[2]); #math.log(float(elem_list[2]));
		poten_si_0[(char_ind,img_id)] = val;
		

# reads the potential si_t from given data file
# all factors must be kept in log space
def readSit(file_name):
	global poten_si_t;
	global num_char;
	global num_img_ids;

	poten_si_t = {};


	file = open(file_name);
	trans_data = file.read();
	file.close();
	trans_list = trans_data.strip().split("\n");
	for elem in trans_list:
		elem_list  = elem.strip().split("\t");
		char_ind0  = char_map[elem_list[0]];
		char_ind1  = char_map[elem_list[1]];
		val 	   = float(elem_list[2]);#math.log(float(elem_list[2]));
		poten_si_t[(char_ind0,char_ind1)] = val;


#################################################################################################################################################

class node:
	id = 0;
	poten     = {};
	belief    = 1.0;
	level     = 0;
	current   = [];
	msg_flag  = {};
	msg_vars  = {};
	msgs      = {};
	ngbrs     = [];


	def __init__(self, id, ngbrs ,current):
		self.id 		 = id;
		self.current 	 = current;
		self.ngbrs     = ngbrs;
		self.msg_flag  = {};

	def print_node(self):
		print "id :: "+str(self.id);
		print "domain :: "+str(self.current);
		print "ngbrs :: "+str(self.ngbrs);
		print "msg_vars ::"+str(self.msg_vars)
		print "msg_flags ::"+str(self.msg_flag)
		# print "msges ::"+str(self.msgs)

#################################################################################################################################################


def configurePoten(observed1,observed2, case):
	potentials   = [];
	actual_poten = [];
	adj_list     = {};
	for i in range(0,len(observed2)+len(observed1)):
		adj_list[i] = [];
	if(case=="ocr" or case=="ocrt" or case=="ocrts" or case=="ocrtsps"):   #check
		for a in range(0,len(observed1)):
			poten = {};
			for i in range(0,num_char):
				# print hash([i])
				poten[(i,)] = poten_si_0[ (i,observed1[a])];
			actual_poten += [poten];
			potentials   += [[a]]; 
				
		for b in range(0,len(observed2)):
			temp = b+len(observed1);
			poten = {};
			for i in range(0,num_char):
				poten[(i,)] = poten_si_0[ (i,observed2[b]) ];
			actual_poten += [poten];
			potentials   += [[temp]]; 

	if(case=="ocrt" or case=="ocrts" or case=="ocrtsps"):				#check
		for a in range(0,len(observed1)-1):
			actual_poten += [poten_si_t];
			potentials += [[a,a+1]];
			adj_list[a] += [a+1];
			adj_list[a+1] += [a];


		for b in range(0,len(observed2)-1):
			t = len(observed1) + b;
			actual_poten += [poten_si_t];
			potentials += [[t,t+1]];
			adj_list[t] += [t+1];
			adj_list[t+1] += [t];

	if(case=="ocrts" or case=="ocrtsps"):
		for a in range(0,len(observed1)):
			for b in range(a+1,len(observed1)):
				if(observed1[a] == observed1[b]):
					actual_poten += [skip_poten];
					potentials += [[a,b]];
					adj_list[a] += [b];
					adj_list[b] += [a];

		for a in range(0,len(observed2)):
			t = a+len(observed1);
			for b in range(a+1,len(observed2)):
				t2 = b+len(observed1);
				if(observed2[a] == observed2[b]):
					actual_poten += [skip_poten];
					potentials += [[t,t2]];
					adj_list[t] += [t2];
					adj_list[t2] += [t];
	if(case=="ocrtsps"):
		for a in range(0,len(observed1)):
			for b in range(0,len(observed2)):
				t = len(observed1)+b;
				if(observed1[a] == observed2[b]):
					actual_poten += [skip_poten];
					potentials += [[a,t]];
					adj_list[a] += [t];
					adj_list[t] += [a];


	return (potentials,actual_poten,adj_list);

def calculatePermute(size):
	global all_comb_mem;
	try:
		return all_comb_mem[size]
	except:
		# print "not found "+str(size);
		if size == 1:
			result = [];
			for i in range(0,len(char_vals)):
				result+=[[char_vals[i]]]
			return result;
		curr = [];
		for c in char_vals:
			possible = calculatePermute(size-1);
			for each in possible:
				curr+=[each+[c]];
		all_comb_mem[size] = curr;
		return curr;

def combine( pot_ranges , act_poten , domain):
	# print domain;
	# start = time.time();
	temp_dict = {};
	for i in range(0,len(domain)):
		temp_dict[domain[i]] = i;
	all_perm = calculatePermute(len(domain))
	result_dict = {};
	# print " level 1 ::  "+str(time.time()-start);
	# start = time.time();

	# pprint(pot_ranges);
	# pprint(act_poten);
	# pprint(all_perm);
	# print(len(all_perm));
	# print(len(act_poten))
	for elem in all_perm:
		for j in range(0,len(act_poten)):
			val = [];
			for k in range(0,len(pot_ranges[j])):
				val += [elem[temp_dict[pot_ranges[j][k]]]];
				# a = hash(elem);
			# print tuple(elem);
			# print tuple(val);
			# pprint(act_poten[j])
			# print act_poten[j][tuple(val)]
				# a = hash(val);
			try:
				result_dict[tuple(elem)] *= act_poten[j][tuple(val)];
			except:
				result_dict[tuple(elem)]  = act_poten[j][tuple(val)];
	# print " level 2 ::  "+str(time.time()-start);
	return result_dict;

def get_ordering(adj_list):
	order = [];
	V = adj_list.keys();
	for i in adj_list.keys():
		adj_list[i] = list(set(adj_list[i]));
	# pprint(adj_list);

	while(len(V)>0):
		min_deg =  1000000;
		min_v   = -1;
		for vertex in V:
			deg = 0;
			try:
				deg = len(adj_list[vertex]);
			except:
				deg = 0;
			if(deg<min_deg):
				min_deg = deg;
				min_v   = vertex;
		V.remove(min_v);
		# pprint(adj_list);
		# exit(0)

		# print min_v
		for i in range(0,len(adj_list[min_v])):
			adj_list[adj_list[min_v][i]].remove(min_v)
			for j in range(i+1,len(adj_list[min_v])):
				adj_list[adj_list[min_v][i]] = list(set(adj_list[adj_list[min_v][i]]) | set([adj_list[min_v][j]]))
				adj_list[adj_list[min_v][j]] = list(set(adj_list[adj_list[min_v][j]]) | set([adj_list[min_v][i]]))
		del adj_list[min_v];
		order += [min_v];

	# print order
	return order;


#################################################################################################################################################

def subsume(node_list):
	new_list = {};
	for i in range(0,len(node_list)):
		new_list[i] = [i]
	marked = [0]*len(node_list);
	for i in range(0,len(node_list)):
		nodet = node_list[i];
		t_set = set(nodet.current);
		for j in range(0,len(nodet.ngbrs)):
			curr = node_list[nodet.ngbrs[j]]
			if(t_set <= set(curr.current)):
				new_list[nodet.ngbrs[j]] += [i];
				marked[i] = 1;							# this tells that i belongs to someone else now and go to that which has 
				break;


	temp_dict = copy.deepcopy(new_list);
	# pprint(new_list)

	for i in temp_dict.keys():
		t = set([])
		try:
			t = set(new_list[i])
		except:
			continue;

		for j in new_list[i]:
			if(j==i):
				continue;
			try:
				a = new_list[j];
				t = t | set(a);
				del new_list[j];
				# print j
			except:
				continue;

		new_list[i] = list(t);
	
	# pprint(new_list)
	sets = new_list.values();
	n_list = [];
	set_map = {};
	for i in range(0,len(sets)):
		n2 = node(i,[],[]);
		act_poten = [];
		poten = []; 
		for j in sets[i]:
			set_map[j] = i;
			n2.current = list(set(n2.current) | set(node_list[j].current));
			n2.ngbrs   = list(set(n2.ngbrs) | set(node_list[j].ngbrs));
			act_poten += [node_list[j].poten];
			poten += [node_list[j].current];
		n2.poten = combine(poten,act_poten,n2.current)
		n_list += [n2]

	for i in range(0,len(n_list)):
		tem = set([]);
		elem = n_list[i];
		for each in elem.ngbrs:
			tem = tem | set([set_map[each]]);
		ngrs = list(tem)
		if(i in ngrs):
			ngrs.remove(i)
		n_list[i].ngbrs = ngrs;

	return n_list



	# one way is to do one at a time recursively 

	# also if edges in the way that -- 



# this will return an array of nodes representing a clique tree based on minfill algorithm
# the input provided specifies a specific markov network with given variables, here the graph
# is visualised as adjusted 
def makeCTree(observed1, observed2, case):		#check
	poten_list   = [];				#marks out the original potentials between elements;
	actual_poten = [];
	node_list    = [];				#nodes of graph based on si;
	msg_list     = [];				#msg passed by nodes which are actually new potentials in variable elimination but kept separate here;
	marked_pot   = [];
	adj_list 	 = {}
	start = time.time()
	(poten_list,actual_poten,adj_list) = configurePoten(observed1,observed2, case);
	# for i in range(0,len(poten_list)):
	# 	print(actual_poten[i]);
	#   print(poten_list[i]);
	# 	# print sum(actual_poten[i].values())
	# print ""
	# print " configurePoten ::  "+str(time.time()-start);
	start = time.time();
	ve_order = get_ordering(adj_list);
	# print " get_ordering ::  "+str(time.time()-start);
	start = time.time();
	# print poten_list;
	# pprint(actual_poten);
	# print ve_order

	marked_pot   = [0]*len(poten_list);
	marked_msg   = [0]*len(ve_order);
	#initialize all the above
	for elem in range(0,len(ve_order)):
		var = ve_order[elem];
		si  = set([]);
		temp_pot   = [];
		temp_act   = [];

		# print marked_pot
		# print poten_list
		n1 = node(len(node_list),[],[]);
		for i in range(0,len(poten_list)):	#covering all the concerned potentials
			if(marked_pot[i]==1):
				continue;
			each = poten_list[i]
			if var in each:
				# print str(var)+" "+str(each)
				si = si | set(each);
				temp_pot+= [poten_list[i]];
				temp_act+= [actual_poten[i]];
				marked_pot[i] = 1;
		# print " stage 1 loop ::  "+str(time.time()-start);
		# print msg_list
		for j in range(0,len(msg_list)):	#covering all the elements
			if(marked_msg[j]==1):
				continue
			if var in msg_list[j]:
				node_list[j].ngbrs += [elem];
				n1.ngbrs += [j];
				si = si | set(msg_list[j]);	
				# print str(var)+" "+str(msg_list[j])
				marked_msg[j] = 1;
		# print " stage 2 loop ::  "+str(time.time()-start);
		# print str(var)+" "+str(temp_pot)+"  "+str(si)

		
		n1.current = list(si);
		t_list     = list(si);
		t_list.remove(var);						 #sexit(0)ince it has been eliminated
		msg_list  += [t_list];		
		n1.poten = combine(temp_pot,temp_act,n1.current);   #of the dimension of the current of the node
		# print n1.id
		# print(n1.poten);
		# print "check"
		# print "check"
		# print "check"
		# print(temp_pot);
		# print "check"

		node_list +=[n1]
		# print " stage 3 loop ::  "+str(time.time()-start);

	# exit(0)
	# node_list = subsume(node_list);
	# print " main loop ::  "+str(time.time()-start);
	start = time.time();
	for each in node_list:
		each.msg_vars = {};
		each.msgs = {};
		for i in each.ngbrs:
			each.msg_flag[i] = 0 ;
			each.msgs[i] = {};
			each.msg_vars[i] = list(set(node_list[i].current) & set(each.current)) 

	# print " 1 ::  "+str(time.time()-start);
	start = time.time();
	# print node_list
	# print "###############################################"
	# for i in node_list:
	# 	i.print_node();
	# print "###############################################"
	# exit(0);
	return node_list;

# this will simply return the cluster graph corresponding to original graphical models
# this should be in a bethe cluster format . One list of nodes , one list of potentials with edges on both sides
def makeClusterGraph(observed1, observed2,case):
	actual_poten = [];
	node_list    = [];				#nodes of graph based on si;
	msg_list     = [];				#msg passed by nodes which are actually new potentials in variable elimination but kept separate here;
	marked_pot   = [];

	(poten_list,actual_poten,adj_list) = configurePoten(observed1,observed2, case);
	# print poten_list
	# creating bethe cluster
	# adding normal vertices
	perms = calculatePermute(1);
	t_dict = {};
	for each in perms:
		t_dict[tuple(each)] = 1.0;

	for i in range(0,len(observed1)+len(observed2)):
		n = node(i,[],[i]);			#current contains ids of nodes while;
		n.poten = copy.deepcopy(t_dict);
		node_list += [n];

	start = len(observed1)+len(observed2);
	#now pushing in the potential nodes
	curr_ind = 0;
	for i in range(0,len(poten_list)):
		curr = actual_poten[i];
		domain = poten_list[i];
		if(len(domain)==1):
			ind = domain[0];
			node_list[ind].poten = curr;
			continue;
		n = node(start+curr_ind,[],domain);
		n.poten = curr;
		for j in domain:					#the ids will match with the node indexes
			node_list[j].ngbrs += [start+curr_ind];
			n.ngbrs += [j];
		node_list += [n];
		curr_ind+=1;

	Edges = [];
	for i in range(0,len(node_list)):
		for j in node_list[i].ngbrs:
			Edges += [(i,j)];

	for j in range(0,len(node_list)):
		node_list[j].msg_vars = {};
		node_list[j].msgs = {};
		for i in node_list[j].ngbrs:
			node_list[j].msg_flag[i] = 1 ;
			node_list[j].msg_vars[i] = list(set(node_list[i].current) & set(node_list[j].current))
			perms = calculatePermute(len(node_list[j].msg_vars[i]));
			node_list[j].msgs[i] = {};
			for elem in perms:
				node_list[j].msgs[i][tuple(elem)] = 1.0;


	# print Edges;

	# print "###############################################"
	# for i in node_list:
	# 	i.print_node();
	# print "###############################################"
	# exit(0);
	return (node_list,Edges);


				
#################################################################################################################################################

def initialise_root(node_list, root):
	queue = [];
	# print root

	marked = [0]*len(node_list);
	parent_ind = [-1]*len(node_list);
	# print "start"
	all_mark = False

	while(not(all_mark)):
		parent_ind[root] = -2;					#marks out those who have no children -- though only one can be root but disjoint so its like this
		node_list[root].level = 0;
		queue += [root];
		while(len(queue)>0):
			front = queue[0];
			queue.pop(0);
			if(marked[front]==1):
				continue;
			# print front
			marked[front] = 1;
			for i in range(0,len(node_list[front].ngbrs)):
				each = node_list[front].ngbrs[i];
				if(marked[each]==0):
					# print "neighbr "+str(each)
					queue += [each];
					node_list[each].level = node_list[front].level+1;
				else:
					parent_ind[front] = node_list[front].ngbrs[i];		#actual parent index
		all_mark = True;
		for i in range(0,len(parent_ind)):
			if(parent_ind[i]==-1):
				root = i;
				all_mark = False;
				break;
				



	# print parent_ind
	return (node_list,parent_ind);

def combine_message(n):
	elems = n.current;
	# print elems
	rev_map = {};
	for i in range(0,len(elems)):
		rev_map[elems[i]] = i;
	# n.print_node()
	# print(n.msgs);
	# print(n.poten)
	# print len(n.poten.keys())
	for ele in n.poten.keys():
		for i in n.msgs.keys():
			if(n.msg_flag[i]!=1):		#either 0 or 2 if message not received or its the parent we are talking about
				continue;
			a = [];
			for j in range(0,len(n.msg_vars[i])):
				a += [ele[rev_map[n.msg_vars[i][j]]]];
			# print str(ele)+"  "+str(a)+"  "
			n.poten[ele] =  n.poten[ele]*n.msgs[i][tuple(a)]
	return n;

def update_belief(n):
	elems = n.current;
	# print elems
	rev_map = {};
	for i in range(0,len(elems)):
		rev_map[elems[i]] = i;
	# n.print_node()
	# print(n.msgs);
	# print(n.poten)
	# print len(n.poten.keys())
	for ele in n.poten.keys():
		n.belief[ele] = n.poten[ele];
		for i in n.msgs.keys():
			# if(n.msg_flag[i]!=1):		#either 0 or 2 if message not received or its the parent we are talking about
			# 	continue;
			a = [];
			for j in range(0,len(n.msg_vars[i])):
				a += [ele[rev_map[n.msg_vars[i][j]]]];
			# print str(ele)+"  "+str(a)+"  "
			n.belief[ele] =  n.belief[ele]*n.msgs[i][tuple(a)]
	return n;

def combine_msg_ind(n,par):
	if(par<0):
		return n;
	elems = n.current;
	rev_map = {};
	for i in range(0,len(elems)):
		rev_map[elems[i]] = i;
	# print "combining parent "+str(par)
	# print "------------------------------------------------------------------"
	# print n.poten
	# print "====------------------------------------------------------------------"
	# print n.msgs[par]
	# print "====------------------------------------------------------------------"

	for ele in n.poten.keys():
			# elem = unhash(ele);
			a = [];
			for j in range(0,len(n.msg_vars[par])):
				a += [ele[rev_map[n.msg_vars[par][j]]]];
			# print ele;
			# print hash(a)
			# print n.poten[ele];
			# print n.msgs[par];
			# print n.msgs[par][tuple(a)];
			n.poten[ele] =  n.poten[ele]*n.msgs[par][tuple(a)]
			# print n.poten[ele];

	# print n.current
	# print n.poten
	# print "------------------------------------------------------------------"

	return n;


def merge(list1 , list2 , order1 ):
	a = [-1]*(len(list1)+len(list2));
	for i in range(0,len(list1)):
		a[order1[i]] = list1[i];
	k = 0;
	for i in range(0,len(a)):
		if a[i]<0:
			a[i] = list2[k];
			k+=1;
	return a;

def make_message_ind(n, ind):
	msg = {};
	pos = [];
	for i in range(0,len(n.msg_vars[ind])):
		elem = n.msg_vars[ind][i];
		pos+=[n.current.index(elem)];

	# n.print_node()

	perms  = calculatePermute(len(n.msg_vars[ind]));
	perms2 = calculatePermute(len(n.current)-len(n.msg_vars[ind]));
	if(len(perms2)==0):
		for each in perms:
			# print n.poten
			# print n.poten[tuple(each)];
			# print n.msgs[ind][tuple(each)]
			msg[tuple(each)] = n.poten[tuple(each)]/n.msgs[ind][tuple(each)];
	else:
		for each in perms:
			msg[tuple(each)] = 0.0;
			for elem in perms2:
				e = merge(each,elem,pos);
				# print msg[tuple(each)]
				# print n.poten[tuple(e)]
				# print n.msgs[ind]
				# print n.msgs[ind][tuple(each)]

				msg[tuple(each)] = msg[tuple(each)]+n.poten[tuple(e)]/n.msgs[ind][tuple(each)];			#removing the old message present in it
	return msg;


def make_message(n, par):
	msg = {};
	pos = [];
	for i in range(0,len(n.msg_vars[par])):
		elem = n.msg_vars[par][i];
		pos+=[n.current.index(elem)];

	perms  = calculatePermute(len(n.msg_vars[par]));
	perms2 = calculatePermute(len(n.current)-len(n.msg_vars[par]));
	# n.print_node()
	# print n.msg_vars[par]
	# print n.current
	if(len(perms2)==0):
		for each in perms:
			msg[tuple(each)] = n.poten[tuple(each)];
	else:
		for each in perms:
			msg[tuple(each)] = 0.0;
			for elem in perms2:
				e = merge(each,elem,pos);
				# print str(each)+" "+str(elem)+str(pos)+" "+str(e);
				msg[tuple(each)] = msg[tuple(each)]+n.poten[tuple(e)];

	return msg;

#this will return the node list with updated beliefs for each node
def CTreeSPComplete( node_list ):
	start = time.time();
	root = len(node_list)-1;
	(node_list,parent_ind) = initialise_root(node_list, root);
	# print "###############################################"
	# for i in node_list:
	# 	print parent_ind[i.id]
	# 	i.print_node();
	# print "###############################################"
	# exit(0);
	ready = [0]*len(node_list);
	for i in range(0,len(node_list)):
		if(len(node_list[i].ngbrs)== 1 and parent_ind[i] > 0 ):		#this ensures that no leaf is taken when it has incoming edge
			ready[i] = 1;	#as leaves so ready
		if(len(node_list[i].ngbrs) == 0):
			ready[i] = 1;	#as leaves so ready
		par = parent_ind[i]

		if(par>=0):
			node_list[i].msg_flag[par] = 2;		#to mark the parent in this setting so message is not required in up pass
	

	if(len(node_list[root].ngbrs)==0):
		ready[root] = 1;

	
	#upwards section
	while(1 in ready):
		for i in range(0,len(ready)):
			if ready[i]==1:
				par = parent_ind[i]
				if(par<0):					#there is no parent and it was ready so no neighbours or root
					ready[i] = 2;
					continue;
				# print "first check";
				# node_list[i].print_node();

				node_list[i] = combine_message(node_list[i]);	#has all messages received update potential -- all messages except parent
				msg = make_message(node_list[i],par);			#make message by summing out potential

				node_list[par].msg_flag[i] = 1;
				node_list[par].msgs[i] = msg;

				check = False;
				for j in node_list[par].msg_flag.values():		#root will have flag 2 so covered
					if(j==0):
						check = True;
						break;
				if(not(check)):
					ready[par] = 1;
				ready[i] = 2;									#done complete
				# print ready
	
	# exit(0)
	#downwards section
	ready_rev = [0]*len(node_list);
	for i in range(0,len(parent_ind)):
		if(parent_ind[i]<0):
			# print str(i)+"  parent"
			ready_rev[i] = 1; 				#need to mark all those that have their messages complete as ready
			node_list[i] = combine_message(node_list[i]);	#the roots never got a chance to send a message so their poten is not combined hence combining
		# print " 3 ::  "+str(time.time()-start);
	start = time.time();
	# print ready_rev

	while(1 in ready_rev):
		for i in range(0,len(ready_rev)):
			par  = parent_ind[i];
			if ready_rev[i] == 1:

				node_list[i] = combine_msg_ind(node_list[i],par);
				for j in range(0,len(node_list[i].ngbrs)):
					curr = node_list[i].ngbrs[j];
					if(curr==par):
						continue;
					msg = make_message_ind(node_list[i],curr);
					node_list[curr].msg_flag[i] = 1;
					node_list[curr].msgs[i] = msg;
					ready_rev[curr] = 1;
					if(0 in node_list[curr].msg_flag.values()):
						print node_list[curr].msg_flag
						exit(0);
				ready_rev[i] = 2;

	start = time.time();
	#updated beliefs at each node
	return node_list;

def predictSP(observed1, observed2,case):
	n_list = makeCTree(observed1, observed2,case);
	n_list = CTreeSPComplete(n_list);

	predict_word_ids = [0]*(len(observed1)+len(observed2));
	marginals = {};
	for i in range(0,len(n_list)):
		node = n_list[i]
		elems = node.current;
		n_marg = [];
		n_marg_ind = {};
		for j in range(0,len(node.current)):
			elem = node.current[j]
			try:
				if( len(marginals[elem]) >= 0):
					continue;
			except:
				marginals[elem] = [0]*num_char;
				n_marg += [elem]
				n_marg_ind[elem] = j;
		# print "node picked :: "+str(i)+" "+str(n_marg)+" "+str(node.current);

		for each in node.poten.keys():
			for p in n_marg:
				index = n_marg_ind[p];
				char = each[index];
				marginals[p][char] += node.poten[each];

	marginals2 = {};
	for i in range(0,len(n_list)):
		node = n_list[len(n_list)-1-i]
		elems = node.current;
		n_marg = [];
		n_marg_ind = {};
		for j in range(0,len(node.current)):
			elem = node.current[j]
			try:
				if( len(marginals2[elem]) >= 0):
					continue;
			except:
				marginals2[elem] = [0]*num_char;
				n_marg += [elem]
				n_marg_ind[elem] = j;

		# print "node picked :: "+str(len(n_list)-1-i)+" "+str(n_marg)+" "+str(node.current);
		for each in node.poten.keys():
			for p in n_marg:
				index = n_marg_ind[p];
				char = each[index];
				marginals2[p][char] += node.poten[each];

	word1 = "";
	word2 = "";
	prob = 1.0;
	for i in range(0,len(observed1)):
		val = max(marginals[i]);
		summed = sum(marginals[i]);
		prob*=val/summed;
		char = marginals[i].index(val);
		word1 += chars[char];

	for i in range(0,len(observed2)):
		val = max(marginals[i+len(observed1)]);
		summed = sum(marginals[i+len(observed1)]);
		prob*=val/summed;
		char = marginals[i+len(observed1)].index(val);
		word2 += chars[char];

	# print word1
	# print word2
	ll = math.log(prob)
	return (word1,word2,ll);


#this will return the node list with updated beliefs for each node
def LoopySPBP( node_list, Edges ):		#edges are checked and the node graph is also correct some problem here only
	messages = {};
	#func initialize CTRee
	for each in node_list:
		each.belief = copy.deepcopy(each.poten);
	mark = {};
	# print Edges
	for each in Edges:
		i = each[0];
		j = each[1];
		try:
			if(mark[each]==0):
				continue;
			else:
				mark[each] = 1;
				mark[(j,i)] = 0;
		except:
			mark[each] = 1;
			mark[(j,i)] = 0;	


	#stopping criteria
	epsilon = 0.001;
	converge = False;
	t = 0;
	diff = 0.0;
	curr_sum = 0.0;
	prev_sum = 0.0;
	while(not(converge)):
		t = t+1;
		curr_sum = 0.0;
		for each in Edges:
			i = each[0];
			j = each[1];
			# print "message :: "+str(each)
			n = copy.deepcopy(node_list[i]);
			n = combine_message(n);							    #potential modified
			node_list[j].msgs[i] = make_message_ind(n,j);		#only message modified
			norm = sum(node_list[j].msgs[i].values());
			if(mark[each]==1):
				for k in node_list[j].msgs[i].keys():
					node_list[j].msgs[i][k]/= norm;


			#
		for i in range(0,len(node_list)):
			node_list[i] = update_belief(node_list[i]);
			curr_sum += sum(node_list[i].belief.values());

		diff = abs(curr_sum-prev_sum);
		# print "difference :: "+str(diff);
		if(diff<epsilon):
			converge = True;
		prev_sum = curr_sum;
	# print "here"
	return node_list;

def predictLoopy(observed1,observed2,case):
	(n_list,Edges) = makeClusterGraph(observed1, observed2,case);
	n_list = LoopySPBP(n_list,Edges);

	predict_word_ids = [0]*(len(observed1)+len(observed2));
	marginals = {};
	for i in range(0,len(n_list)):
		node = n_list[i]
		elems = node.current;
		n_marg = [];
		n_marg_ind = {};
		for j in range(0,len(node.current)):
			elem = node.current[j]
			try:
				if( len(marginals[elem]) >= 0):
					continue;
			except:
				marginals[elem] = [0]*num_char;
				n_marg += [elem]
				n_marg_ind[elem] = j;
		# print "node picked :: "+str(i)+" "+str(n_marg)+" "+str(node.current);

		for each in node.belief.keys():
			for p in n_marg:
				index = n_marg_ind[p];
				char = each[index];
				marginals[p][char] += node.belief[each];

	word1 = "";
	word2 = "";
	prob = 1.0;
	for i in range(0,len(observed1)):
		val = max(marginals[i]);
		summed = sum(marginals[i]);
		prob*=val/summed;
		char = marginals[i].index(val);
		word1 += chars[char];

	for i in range(0,len(observed2)):
		val = max(marginals[i+len(observed1)]);
		summed = sum(marginals[i+len(observed1)]);
		prob*=val/summed;
		char = marginals[i+len(observed1)].index(val);
		word2 += chars[char];

	ll = math.log(prob)
	return (word1,word2,ll);

#################################################################################################################################################

def predict_words(image_file,word_file):
	# print "correct the file reading for two words"
	file_img = open(image_file);
	data_img = file_img.read();
	file_img.close();
	file_word = open(word_file);
	data_word = file_word.read();
	file_word.close();

	img_list = data_img.strip().split("\n\n");
	word_list = data_word.strip().split("\n\n");

	
	total_char   = 0;
	correct_char = 0;
	total_word   = 0;
	correct_word = 0;
	loglikelihood= 0;

	for id in range(0,len(img_list)):
		observed_list = img_list[id].strip().split("\n");
		observed1 = map(int,observed_list[0].strip().split("\t"));
		observed2 = map(int,observed_list[1].strip().split("\t"));

		w_list = word_list[id].strip().split("\n");
		word1 = w_list[0];
		word2 = w_list[1];

		(predict_word1,predict_word2,ll) = predictSP(observed1,observed2,"ocrtsps");
		# (predict_word1,predict_word2,ll) = predictLoopy(observed1,observed2,"ocrtsps");
		# (predict_word1,predict_word2,ll) = predictSP(observed1,observed2,"ocrt");
		# (predict_word1,predict_word2,ll) = predictSP(observed1,observed2,"ocrts");
		# (predict_word1,predict_word2,ll) = predictSP(observed1,observed2,"ocrtsps");
		
		loglikelihood+= ll
		# print word1+" "+predict_word1
		# print word2+" "+predict_word2
		total_word += 1;
		if(predict_word1==word1):
			correct_word += 1;
		for ind in range(0,len(predict_word1)):
			total_char+=1;
			if(word1[ind]==predict_word1[ind]):
				correct_char+=1;

		total_word += 1;
		if(predict_word2==word2):
			correct_word += 1;
		for ind in range(0,len(predict_word2)):
			total_char+=1;
			if(word2[ind]==predict_word2[ind]):
				correct_char+=1;

	print "word wise accuracy : "+str(float(correct_word)/float(total_word));
	print "character wise accuracy : "+str(float(correct_char)/float(total_char));
	print "loglikelihood : "+str(loglikelihood);
	print "Average loglikelihood : "+str(loglikelihood/float(total_word));


if __name__ == '__main__':
	all_comb_mem[0] = [];
	readSi0("OCRdataset-2/potentials/ocr.dat");
	readSit("OCRdataset-2/potentials/trans.dat");
	for a in range(0,num_char):
		for b in range(0,num_char):
			if(a==b):
				skip_poten[(a,b)] = 5;
			else:
				skip_poten[(a,b)] = 1;


	# print len(poten_si_0);
	# print len(poten_si_0[0]);
	# pprint(poten_si_0)
	# print len(poten_si_t);
	# print len(poten_si_t[0]);
	# pprint(poten_si_t)
	start =  time.time();

	# print calculateScoreOCR([582,969,582,969],"adad");
	# print calculateScoreTrans([582,969,582,969],"adad");
	# print calculateScoreComb([582,969,582,969],"adad");
	# print predictSP([582,969,582,969], [582,969,582,969], "ocr");
	# print predictSP([582,969,582,969], [582,969,582,969], "ocrtsps");
	# print predictSP([582,969,582,969], [582,969,582,969], "ocrts");
	# print "ok"
	# print predictLoopy([582,969,582,969], [582,969,582,969], "ocrts");
	# makeClusterGraph([582,969,582,969], [582,969,582,969], "ocrtsps");
	predict_words("OCRdataset-2/data/data-tree.dat","OCRdataset-2/data/truth-tree.dat")
	print str(time.time()-start);
	start =  time.time();
	predict_words("OCRdataset-2/data/data-treeWS.dat","OCRdataset-2/data/truth-treeWS.dat")
	print str(time.time()-start);
	start =  time.time();
	predict_words("OCRdataset-2/data/data-loops.dat","OCRdataset-2/data/truth-loops.dat")
	print str(time.time()-start);
	start =  time.time();
	predict_words("OCRdataset-2/data/data-loopsWS.dat","OCRdataset-2/data/truth-loopsWS.dat")
	# jumbo = 0
	# # m = {}
	# for i in range(0,2000000):
	# 	# a = hash([1,3,4,5,5])
	# 	# m[str([1,3,4,5,5])] = 1;
	# 	a = tuple([1,2,3])
	# 	jumbo += 1;

	# print node_list
	# CTreeSPComplete(node_list);
	# predict_words("OCRdataset/data/small/images.dat","OCRdataset/data/small/words.dat")
	print str(time.time()-start);



#ocr checked
#################################################################################################################################################

#approach
# 2. create a clique tree datastructure -- node contains : self potential , children , parent , msg_flag , belief , msg
# 3. implement message passing



# first complete ocrts and ocrtsps --*
# complete predict words --*
# then complete loopy
# then finish map inference and compare and debug

