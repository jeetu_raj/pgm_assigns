import math;
from pprint import pprint;
from multiprocessing import Pool
import copy
import time
import json;
import numpy as np;
import random;

num_char = 10;
num_img_ids = 1000;

poten_si_0 = {};		#10*1000
poten_si_t = {};		#10*10

all_comb_mem = {};


char_map   = {'e':0,'t':1,'a':2,'o':3,'i':4,'n':5,'s':6,'h':7,'r':8,'d':9};
chars	   = ['e','t','a','o','i','n','s','h','r','d'];
char_vals  = [0,1,2,3,4,5,6,7,8,9]

skip_cons  = 5;
skip_poten = {};

#################################################################################################################################################

def hash(list): 
	return json.dumps(list);

def unhash(string): 
	return json.loads(string);

# reads the potential si_0 from given data file
# all factors must be kept in log space
def readSi0(file_name):
	global poten_si_0;
	global num_char;
	global num_img_ids;

	poten_si_0 = {};

	file = open(file_name);
	ocr_data = file.read();
	file.close();
	ocr_list = ocr_data.strip().split("\n");
	for elem in ocr_list:
		elem_list = elem.strip().split("\t");
		char_ind  = char_map[elem_list[1]];
		img_id    = int(elem_list[0]);
		val 	  = float(elem_list[2]); #math.log(float(elem_list[2]));
		poten_si_0[(char_ind,img_id)] = val;
		

# reads the potential si_t from given data file
# all factors must be kept in log space
def readSit(file_name):
	global poten_si_t;
	global num_char;
	global num_img_ids;

	poten_si_t = {};


	file = open(file_name);
	trans_data = file.read();
	file.close();
	trans_list = trans_data.strip().split("\n");
	for elem in trans_list:
		elem_list  = elem.strip().split("\t");
		char_ind0  = char_map[elem_list[0]];
		char_ind1  = char_map[elem_list[1]];
		val 	   = float(elem_list[2]);#math.log(float(elem_list[2]));
		poten_si_t[(char_ind0,char_ind1)] = val;


#################################################################################################################################################

class var:
	id = 0;
	assign  = 0;
	range = 0;				#assuming all are integral assignments

	def __init__(self, id, assign ,range):
		self.id 		 = id;
		self.assign 	 = assign;
		self.range     = range;


	def print_node(self):
		print "id :: "+str(self.id);
		print "assign :: "+str(self.assign);
		print "range :: "+str(self.range);

class poten:
	id = 0;
	val = {};
	val_par_bn = {};
	domain = [];
	domain_map = [];

	def __init__(self, id, val ,domain):
		self.id 		 = id;
		self.val 	 = val;
		self.domain     = domain;


	def print_node(self):
		print "id :: "+str(self.id);
		pprint(self.val);
		print "domain :: "+str(self.domain);


#################################################################################################################################################
prob_assign = {};

def configurePoten(observed1,observed2, case):
	potentials   = [];
	actual_poten = [];
	if(case=="ocr" or case=="ocrt" or case=="ocrts" or case=="ocrtsps"):   #check
		for a in range(0,len(observed1)):
			poten = {};
			for i in range(0,num_char):
				# print hash([i])
				poten[(i,)] = poten_si_0[ (i,observed1[a])];
			actual_poten += [poten];
			potentials   += [[a]]; 
				
		for b in range(0,len(observed2)):
			temp = b+len(observed1);
			poten = {};
			for i in range(0,num_char):
				poten[(i,)] = poten_si_0[ (i,observed2[b]) ];
			actual_poten += [poten];
			potentials   += [[temp]]; 

	if(case=="ocrt" or case=="ocrts" or case=="ocrtsps"):				#check
		for a in range(0,len(observed1)-1):
			actual_poten += [poten_si_t];
			potentials += [[a,a+1]];


		for b in range(0,len(observed2)-1):
			t = len(observed1) + b;
			actual_poten += [poten_si_t];
			potentials += [[t,t+1]];

	if(case=="ocrts" or case=="ocrtsps"):
		for a in range(0,len(observed1)):
			for b in range(a+1,len(observed1)):
				if(observed1[a] == observed1[b]):
					actual_poten += [skip_poten];
					potentials += [[a,b]];

		for a in range(0,len(observed2)):
			t = a+len(observed1);
			for b in range(a+1,len(observed2)):
				t2 = b+len(observed1);
				if(observed2[a] == observed2[b]):
					actual_poten += [skip_poten];
					potentials += [[t,t2]];

	if(case=="ocrtsps"):
		for a in range(0,len(observed1)):
			for b in range(0,len(observed2)):
				t = len(observed1)+b;
				if(observed1[a] == observed2[b]):
					actual_poten += [skip_poten];
					potentials += [[a,t]];



	return (potentials,actual_poten);

def calculatePermute(size):
	global all_comb_mem;
	try:
		return all_comb_mem[size]
	except:
		# print "not found "+str(size);
		if size == 1:
			result = [];
			for i in range(0,len(char_vals)):
				result+=[[char_vals[i]]]
			return result;
		curr = [];
		for c in char_vals:
			possible = calculatePermute(size-1);
			for each in possible:
				curr+=[each+[c]];
		all_comb_mem[size] = curr;
		return curr;

def randomSample(var_array):	#just for initialisation
	sample = [];
	for i in range(0,len(var_array)):
		x = random.randint(0,len(var_array[i].range)-1);
		# sample += [x];
		sample += [var_array[i].range[x]];
	return sample;


def getProb(sample_array,poten_array):			#make change # need to consider only the potentials that contain the variable which is getting varied
	try:
		val = prob_assign[tuple(sample_array)];
		return val;
	except:
		val = 1.0;
		for each in poten_array:
			var_assign = [];
			for i in range(0,len(each.domain)):
				var_assign += [sample_array[each.domain[i]]];
			val *= each.val[tuple(var_assign)];
		prob_assign[tuple(sample_array)] = val;
		return val;


def condProbSamp(var_array,sample_array,poten_array,id_cond):
	var_cond = var_array[id_cond];
	new_prob = {};
	for i in range(0,len(var_cond.range)):
		# var_array[id_cond].assign = i;
		elem = var_cond.range[i];
		sample_array[id_cond] = elem;
		new_prob[elem] = getProb(sample_array,poten_array);

	Z_cond = sum(new_prob.values());
	pick = random.random();
	keys = new_prob.keys();
	curr = 0;
	for i in range(0,len(keys)):
		prob = new_prob[keys[i]]/Z_cond;
		curr += prob;
		if(pick<=curr):
			return keys[i];
	# return random.randint(0,9)
	return -1;


def gibbsSample(var_array,poten_array):
	global prob_assign;
	prob_assign = {};
	t = 0;
	sample_array = randomSample(var_array);
	burn_in_over = False;
	n = len(var_array);
	T = n*1000;
	R = n*200;
	sample_set = [];
	for i in range(len(sample_array)):
		temp = {};
		sample_set += [temp];


	while(not(burn_in_over) or n*t<T):
		t+=1;
		# print t;
		for id in range(0,n):
			sample = condProbSamp(var_array,sample_array,poten_array,id);
			sample_array[id] = sample;
			if(burn_in_over):
				for i in range(0,len(sample_array)):
					try:
						sample_set[i][sample_array[i]] += 1;
					except:
						sample_set[i][sample_array[i]] = 1;
		if(not(burn_in_over)):
			if(n*t>R):
				burn_in_over = True;
				t = 1;

	# pprint(sample_set);
	# exit(0)
	prob = 1.0;
	max_assign = [];
	# sum_val = 0;
	# for each in sample_set.keys():
	# 	sum_val += sample_set[each];
	# 	if(sample_set[each]>1000):
	# 		print str(each)+" "+str(sample_set[each])
	# 	if(sample_set[each]>max_val):
	# 		max_assign = each;
	# 		max_val = sample_set[each];

	for i in range(0,len(sample_set)):
		max_id = 0;
		max_val = 0;
		sum_val = sum(sample_set[i].values());
		for key in sample_set[i].keys():
			if sample_set[i][key] > max_val:
				max_id = key;
				max_val = sample_set[i][key];
		prob *= max_val/float(sum_val)
		max_assign += [max_id];



	return max_assign,max_val/float(sum_val);



#################################################################################################################################################

def predictGibbs(observed1,observed2):
	 (potentials,actual_poten) = configurePoten(observed1,observed2,"ocrtsps");
	 # pprint(potentials)
	 var_array = [];
	 for i in range(0,len(observed1)):
	 	temp = var(i,0,char_vals);
	 	var_array += [temp];
	 for i in range(0,len(observed2)):
	 	temp = var(i+len(observed1),0,char_vals);
	 	var_array += [temp];

	 poten_array = [];
	 for i in range(0,len(potentials)):
	 	# print(potentials[i])
	 	temp = poten(i,actual_poten[i],potentials[i]);
	 	poten_array += [temp]


	 val,prob = gibbsSample(var_array,poten_array);

	 word1 = ""; 
	 word2 = "";
	 for i in range(0,len(observed1)): 
	 	word1 += chars[val[i]];
	 for i in range(0,len(observed2)): 
	 	word2 += chars[val[i+len(observed1)]];

	 return word1,word2,prob;


def predict_words(image_file,word_file):
	# print "correct the file reading for two words"
	file_img = open(image_file);
	data_img = file_img.read();
	file_img.close();
	file_word = open(word_file);
	data_word = file_word.read();
	file_word.close();

	img_list = data_img.strip().split("\n\n");
	word_list = data_word.strip().split("\n\n");

	
	total_char   = 0;
	correct_char = 0;
	total_word   = 0;
	correct_word = 0;
	loglikelihood= 0;

	for id in range(0,len(img_list)):
		observed_list = img_list[id].strip().split("\n");
		observed1 = map(int,observed_list[0].strip().split("\t"));
		observed2 = map(int,observed_list[1].strip().split("\t"));

		w_list = word_list[id].strip().split("\n");
		word1 = w_list[0];
		word2 = w_list[1];

		predict_word1,predict_word2,prob = predictGibbs(observed1,observed2);
		print id
		# loglikelihood+= CalcProb(observed1,observed2,word1,word2,"ocrtsps");
		# print word1+" "+predict_word1
		# print word2+" "+predict_word2
		# exit(0)
		total_word += 1;
		if(predict_word1==word1):
			correct_word += 1;
		for ind in range(0,len(predict_word1)):
			total_char+=1;
			if(word1[ind]==predict_word1[ind]):
				correct_char+=1;

		total_word += 1;
		if(predict_word2==word2):
			correct_word += 1;
		for ind in range(0,len(predict_word2)):
			total_char+=1;
			if(word2[ind]==predict_word2[ind]):
				correct_char+=1;

	print "word wise accuracy : "+str(float(correct_word)/float(total_word));
	print "character wise accuracy : "+str(float(correct_char)/float(total_char));
	print "loglikelihood : "+str(loglikelihood);
	print "Average loglikelihood : "+str(loglikelihood/float(total_word));


def predict_ocr():
	all_comb_mem[0] = [];
	readSi0("OCRdataset-2/potentials/ocr.dat");
	readSit("OCRdataset-2/potentials/trans.dat");
	for a in range(0,num_char):
		for b in range(0,num_char):
			if(a==b):
				skip_poten[(a,b)] = 5;
			else:
				skip_poten[(a,b)] = 1;


	start =  time.time();
	# print predictGibbs([582,969,582,969], [582,969,582,969]);
	# print predictGibbs([597,992,188,669,570,525], [597,623,295,570,286,221]);


	predict_words("OCRdataset-2/data/data-tree.dat","OCRdataset-2/data/truth-tree.dat")
	# print str(time.time()-start);
	# start =  time.time();
	# predict_words("OCRdataset-2/data/data-treeWS.dat","OCRdataset-2/data/truth-treeWS.dat")
	# print str(time.time()-start);
	# start =  time.time();
	# predict_words("OCRdataset-2/data/data-loops.dat","OCRdataset-2/data/truth-loops.dat")
	# print str(time.time()-start);
	# start =  time.time();
	# predict_words("OCRdataset-2/data/data-loopsWS.dat","OCRdataset-2/data/truth-loopsWS.dat")

	print str(time.time()-start);

# predict_ocr();
# exit(0);


# *gibbs sampler and inference and learning--

# - randomSampler - require the variables and range of values -- assign samples:
# - CPTS are present
# - calculate conditional probability on demand given some variables -- conditional is chill as Z not required
# - sample the next state
# - loop

# *conditional probability calculation--

# - all potentials present,  assignment provided and calculation performed -- and samples collected
# most probable assignment is the one that occurs most popularly

#################################################################################################################################################

#parse bif -- today
#network will have edges but in actual what we need for gibbs sampling is the list of potentials -
#variables with range
# -- after that just the function for calulating the potentials
#setup bayesian learning -- today
#setup markov learning
import bif_parser
import imp
bn = -1;

alpha = 1.0;

def remove_elem(tup,ind):
	temp =  list(tup);
	temp.pop(ind);
	return tuple(temp);

def read_network(file_name):
	global bn;

	file = open(file_name+".bif");
	data = file.read();
	file.close();
	data = data.replace("?","0.0");
	file = open(file_name+".bif",'w');
	file.write(data);
	file.close();
	bif_parser.parse(file_name);

	bn = imp.load_source('bn',file_name+"_bn.py");
	var_array = [];
	poten_array = [];
	id_map = {};
	# print len(bn.var_names);
	# print len(bn.functions)
	# print len(bn.dictionaries)
	# exit(0)
	for i in range(0,len(bn.var_names)):
		id_map[bn.var_names[i]] = i;

	for i in range(0,len(bn.var_names)):
		var_name = bn.var_names[i];
		domain = bn.domains_dict[var_name];
		temp = var(i,domain[0],domain);
		var_array += [temp]
		
		domain_poten = [];
		domain_names = list(bn.functions[i].__code__.co_varnames);
		for j in range(0,len(domain_names)):
			domain_poten += [id_map[domain_names[j]]]
		# print str(domain_names)+" "+str(i)

		ind = domain_names.index(var_name);
		temp_p = poten(i,bn.dictionaries[i],domain_names);
		temp_p.domain_map = domain_poten;
		# pprint(bn.dictionaries[i])
		if(len(domain_names)>1):
			# pprint(bn.dictionaries[i])
			par_poten = {remove_elem(k,ind):0 for (k,v) in bn.dictionaries[i].iteritems()}
			# pprint(par_poten)
			temp_p.val_par_bn = par_poten
		poten_array +=[temp_p];
		
	return (var_array,poten_array,id_map); 	


#################################################################################################################################################
prob_assign_BN = {};


def randomSampleBN(var_array):	#just for initialisation
	sample = [];
	for i in range(0,len(var_array)):
		x = random.randint(0,len(var_array[i].range)-1);
		# sample += [x];
		sample += [var_array[i].range[x]];
	return sample;


def getProbBN(sample_array,poten_array):			#make change # need to consider only the potentials that contain the variable which is getting varied
	try:
		val = prob_assign_BN[tuple(sample_array)];
		return val;
	except:
		val = 1.0;
		for each in poten_array:
			var_assign = [];
			for i in range(0,len(each.domain_map)):
				var_assign += [sample_array[each.domain_map[i]]];
			val *= each.val[tuple(var_assign)];
		prob_assign_BN[tuple(sample_array)] = val;
		return val;

def condProbSampBN(var_array,sample_array,poten_array,id_cond):
	var_cond = var_array[id_cond];
	new_prob = {};
	for i in range(0,len(var_cond.range)):
		# var_array[id_cond].assign = i;
		elem = var_cond.range[i];
		sample_array[id_cond] = elem;
		new_prob[elem] = getProbBN(sample_array,poten_array);

	Z_cond = sum(new_prob.values());
	pick = random.random();
	keys = new_prob.keys();
	curr = 0;
	for i in range(0,len(keys)):
		prob = new_prob[keys[i]]/Z_cond;
		curr += prob;
		if(pick<=curr):
			return keys[i];
	# return random.randint(0,9)
	return -1;

def gibbsSampleBN(var_array,poten_array,given_values):
	global prob_assign_BN;
	prob_assign_BN = {};
	t = 0;
	sample_array = randomSample(var_array);
	burn_in_over = False;
	n = len(var_array);

	given_map = {};
	for i in range(0,n):
		given_map[i] = 0;
		if i in given_values:
			given_map[i] = 1;
			sample_array[i] = var_array[i].assign;


	T = n*1000;
	R = n*200;
	sample_set = [];
	for i in range(len(sample_array)):
		temp = {};
		sample_set += [temp];


	while(not(burn_in_over) or n*t<T):
		t+=1;
		# print t;
		for id in range(0,n):
			if(given_map[id]==1):
				continue;
			sample = condProbSampBN(var_array,sample_array,poten_array,id);
			sample_array[id] = sample;
			if(burn_in_over):
				for i in range(0,len(sample_array)):
					if(given_map[i]==1):
						continue;
					try:
						sample_set[i][sample_array[i]] += 1;
					except:
						sample_set[i][sample_array[i]] = 1;
		if(not(burn_in_over)):
			if(n*t>R):
				burn_in_over = True;
				t = 1;

	# print given_values;
	# print sample_array;
	# pprint(sample_set);
	# exit(0);
	prob = 1.0;
	max_assign = [];

	for i in range(0,len(sample_set)):
		if(given_map[i]==1):
			max_assign += [var_array[i].assign];
			continue;
		max_id = 0;
		max_val = 0;
		sum_val = sum(sample_set[i].values());
		for key in sample_set[i].keys():
			if sample_set[i][key] > max_val:
				max_id = key;
				max_val = sample_set[i][key];
		prob *= max_val/float(sum_val)
		max_assign += [max_id];



	return max_assign,max_val/float(sum_val);

#################################################################################################################################################


def learn_bayesian(data_file,var_array,poten_array):
	global bn;
	file = open(data_file);
	data = file.read();
	file.close();
	data = data.strip().split("\n");
	labels = data[0].strip().split(" ");
	data  = data[1:];
	dict_label = {};
	M = 10000;

	for i in range(0,len(labels)):
		dict_label[labels[i]] = i;
	# pprint(dict_label)
	for i in range(0,M):
		if(i%1000==0):
			print i;
		line = data[i].strip().split(" ");
		for j in range(0,len(poten_array)): 
			comb = [];
			for k in range(0,len(poten_array[j].domain)):
				var = poten_array[j].domain[k]
				comb += [line[dict_label[var]]];
				# print var+" "+str(line[dict_label[var]])

			comb = tuple(comb)

			# pprint(tuple(comb));
			# pprint(poten_array[j].domain)
			# pprint(poten_array[j].val)
			# pprint(poten_array[j].val[tuple(comb)])
			# if(len(comb)==1):
			# 	comb = comb[0];

			if(poten_array[j].val[comb] < 0):
				poten_array[j].val[comb] = 0;
			poten_array[j].val[comb] += 1

			if(len(poten_array[j].domain)>1):
				comb = list(comb);
				ind = poten_array[j].domain.index(bn.var_names[j])
				comb.pop(ind);
				comb = tuple(comb)
				# print bn.var_names[j];
				# pprint(poten_array[j].domain)
				# pprint(comb)
				# pprint(poten_array[j].val)
				# pprint(poten_array[j].val_par_bn)
				poten_array[j].val_par_bn[comb] +=1;


	samples = len(data);
	for j in range(0,len(poten_array)):
		own_ind = poten_array[j].domain.index(bn.var_names[j]);
		range_var = len(var_array[j].range);
		if(len(poten_array[j].domain) > 1):
			for k in poten_array[j].val.keys():
				par_k = copy.deepcopy(list(k));
				par_k.pop(own_ind);
				par_k = tuple(par_k)

				parent_val = poten_array[j].val_par_bn[par_k]
				poten_array[j].val[k] = (poten_array[j].val[k]+alpha)/float(parent_val+range_var*alpha);		#do smoothing right
		else:
			for k in poten_array[j].val.keys():
				poten_array[j].val[k] = (poten_array[j].val[k]+alpha)/float(samples+range_var*alpha);		#do smoothing right

	return poten_array;

def predict_bayesian(infer_file,map_file,var_array,poten_array,id_map):
	file = open(infer_file);
	data = file.read();
	file.close();
	data = data.strip().split("\n");
	labels = data[0].strip().split(" ");
	data = data[1:];

	file = open(map_file);
	actual = file.read();
	file.close();
	actual = actual.strip().split("\n");
	actual = actual[1:];						#assumption-consisting of same mapping as above

	elem = 0;
	total_elem = 0;
	full = 0;
	for i in range(0,len(data)):
		line = data[i].strip().split(" ");
		actual_val = actual[i].strip().split(" ");
		given_val = [];
		for j in range(0,len(line)):
			if(line[j]!="?"):
				index = id_map[labels[j]];
				given_val += [index];
				var_array[index].assign = line[j];
		(predict,prob) = gibbsSampleBN(var_array,poten_array,given_val); 
		

		check = 1;
		check_array = [];
		for k in range(0,len(actual_val)):
			ind_pred = id_map[labels[k]];
			if(ind_pred in given_val):
				continue;
			check_array += [predict[ind_pred]];
			total_elem += 1;
			if(predict[ind_pred]==actual_val[k]):
				elem+=1;
			else:
				# print predict[ind_pred]
				# print actual_val[k]
				check = 0;
		if(check == 1):
			full+=1;
		# print actual_val;
		# print check_array;
		# print "------"
	print total_elem
	print elem/float(total_elem);
	print full/float(len(data))


# (var_array,poten_array,id_map) = read_network("A3-data/A3-data/hepar2");
# (var_array,poten_array,id_map) = read_network("A3-data/A3-data/insurance");
(var_array,poten_array,id_map) = read_network("A3-data/A3-data/andes");
print "network read"
# poten_array = learn_bayesian("A3-data/A3-data/hepar2.dat",var_array,poten_array);
# # for i in range(0,len(var_array)):
# # 	poten_array[i].print_node();
# predict_bayesian("A3-data/hepar_test.dat","A3-data/hepar_TrueValues.dat",var_array,poten_array,id_map);
# exit(0)
#################################################################################################################################################
prob_assign_Markov = {};



def getProbMarkov(sample_array,poten_array,id):			#make change # need to consider only the potentials that contain the variable which is getting varied
	# try:
	# 	val = prob_assign_Markov[tuple(sample_array)];
	# 	return val;
	# except:
	val = 0.0;
	# print sample_array
	for each in poten_array:
		var_assign = [];
		if(id not in each.domain_map):
			continue;
		for i in range(0,len(each.domain_map)):
			var_assign += [sample_array[each.domain_map[i]]];
		val += each.val[tuple(var_assign)];
		# prob_assign_Markov[tuple(sample_array)] = math.exp(val);
	return math.exp(val);

def condProbSampMarkov(var_array,sample_array,poten_array,id_cond):
	var_cond = var_array[id_cond];
	new_prob = {};
	for i in range(0,len(var_cond.range)):
		# var_array[id_cond].assign = i;
		elem = var_cond.range[i];
		sample_array[id_cond] = elem;
		new_prob[elem] = getProbMarkov(sample_array,poten_array,id_cond);
	# pprint(new_prob)
	Z_cond = sum(new_prob.values());
	pick = random.random();
	keys = new_prob.keys();
	curr = 0;
	for i in range(0,len(keys)):
		prob = new_prob[keys[i]]/Z_cond;
		curr += prob;
		if(pick<=curr):
			return keys[i];
	# return random.randint(0,9)
	return -1;

def gibbsSampleMarkov(var_array,poten_array,given_values):
	global prob_assign_Markov;
	prob_assign_Markov = {};
	t = 0;
	sample_array = randomSampleBN(var_array);
	burn_in_over = False;
	n = len(var_array);

	given_map = {};
	for i in range(0,n):
		given_map[i] = 0;
		if i in given_values:
			given_map[i] = 1;
			sample_array[i] = var_array[i].assign;


	T = n*1000;
	R = n*200;
	sample_set = [];
	for i in range(len(sample_array)):
		temp = {};
		sample_set += [temp];


	while(not(burn_in_over) or n*t<T):
		t+=1;
		# print t;
		for id in range(0,n):
			if(given_map[id]==1):
				continue;
			sample = condProbSampMarkov(var_array,sample_array,poten_array,id);
			sample_array[id] = sample;
			if(burn_in_over):
				for i in range(0,len(sample_array)):
					if(given_map[i]==1):
						continue;
					try:
						sample_set[i][sample_array[i]] += 1;
					except:
						sample_set[i][sample_array[i]] = 1;
		if(not(burn_in_over)):
			if(n*t>R):
				burn_in_over = True;
				t = 1;

	# print given_values;
	# print sample_array;
	# pprint(sample_set);
	# exit(0);
	prob = 1.0;
	max_assign = [];

	for i in range(0,len(sample_set)):
		if(given_map[i]==1):
			max_assign += [var_array[i].assign];
			continue;
		max_id = 0;
		max_val = 0;
		sum_val = sum(sample_set[i].values());
		for key in sample_set[i].keys():
			if sample_set[i][key] > max_val:
				max_id = key;
				max_val = sample_set[i][key];
		prob *= max_val/float(sum_val)
		max_assign += [max_id];



	return max_assign,max_val/float(sum_val);

def sample_infer(expec_poten,curr_poten,var_array):
	global prob_assign_Markov;
	prob_assign_Markov = {};
	t = 0;
	sample_array = randomSampleBN(var_array);
	burn_in_over = False;
	n = len(var_array);
	T = n*300;
	R = n*100;
	# T = n*1000; -- hepar insurance
	# R = n*100;

	while(not(burn_in_over) or n*t<T):
		t+=1;
		# print t;
		for id in range(0,n):
			sample = condProbSampMarkov(var_array,sample_array,curr_poten,id);
			sample_array[id] = sample;
			if(burn_in_over):
				for j in range(0,len(expec_poten)): 
					comb = [];
					for k in range(0,len(expec_poten[j].domain_map)):
						var = expec_poten[j].domain_map[k]
						comb += [sample_array[var]];
					# print sample_array;
					# print expec_poten[j].domain_map;
					# print comb;
					# print expec_poten[j].val.keys()[0];
					if(expec_poten[j].val[tuple(comb)] < 0):
						expec_poten[j].val[tuple(comb)] = 0;
					expec_poten[j].val[tuple(comb)] += 1

		if(not(burn_in_over)):
			if(n*t>R):
				burn_in_over = True;
				t = 1;

	for j in range(0,len(expec_poten)): 
		for k in expec_poten[j].val.keys():
			expec_poten[j].val[k] /= float(T);
	# for i in range(0,len(var_array)):
	# 	expec_poten[i].print_node();
	# exit(0);
	return expec_poten;

def learn_markov(data_file,var_array,poten_array):
	global bn;
	file = open(data_file);
	data = file.read();
	file.close();
	data = data.strip().split("\n");
	labels = data[0].strip().split(" ");
	data  = data[1:];
	dict_label = {};


	for i in range(0,len(labels)):
		dict_label[labels[i]] = i;

	# pprint(dict_label)
	eta = 1.0;
	M   = 10000;
	C   = 1.0;
	data_poten = copy.deepcopy(poten_array);	
	#data one time
	for i in range(0,M):
		if(i%1000==0):
			print i;
		line = data[i].strip().split(" ");
		for j in range(0,len(poten_array)): 
			comb = [];
			for k in range(0,len(poten_array[j].domain)):
				var = data_poten[j].domain[k]
				comb += [line[dict_label[var]]];
			comb = tuple(comb)

			# if(len(comb)==1):
			# 	comb = comb[0];

			if(data_poten[j].val[comb] < 0):
				data_poten[j].val[comb] = 0;
			data_poten[j].val[comb] += 1


	curr_poten = copy.deepcopy(poten_array);				#lambdas --- log of potential values 

	for j in range(0,len(curr_poten)): 
		for k in curr_poten[j].val.keys():
			# curr_poten[j].val[k] = (data_poten[j].val[k]+alpha)/float(M);
			curr_poten[j].val[k] = random.random()+1.0;

	# curr_poten = learn_bayesian("A3-data/A3-data/insurance.dat",var_array,poten_array);


	# exit(0);
	#gradient descent
	change = 10000.0;
	epsilon = 0.1;
	t = 0;
	veta = eta;
	while(change>epsilon and t<10):	#convergence criteria is the change in parameters
		print t;
		print change
		t+=1;
		expec_poten = copy.deepcopy(poten_array);
		expec_poten = sample_infer(expec_poten,curr_poten,var_array);
		change = 0;
		eta = veta/t;
		for i in range(0,len(curr_poten)):
			for each in curr_poten[i].val.keys():
				d_reg = -1*C*curr_poten[i].val[each];
				diff = (((data_poten[i].val[each]) + d_reg)/float(M)- expec_poten[i].val[each])
				delta = eta*diff
				curr_poten[i].val[each] = curr_poten[i].val[each] + delta;
				# if(abs(diff)>change):
				change += abs(diff);
		# curr_poten[25].print_node()
	return curr_poten;

def predict_markov(infer_file,map_file,var_array,poten_array,id_map):
	file = open(infer_file);
	data = file.read();
	file.close();
	data = data.strip().split("\n");
	labels = data[0].strip().split(" ");
	data = data[1:];

	file = open(map_file);
	actual = file.read();
	file.close();
	actual = actual.strip().split("\n");
	actual = actual[1:];						#assumption-consisting of same mapping as above

	elem = 0;
	total_elem = 0;
	full = 0;
	for i in range(0,len(data)):
		line = data[i].strip().split(" ");
		actual_val = actual[i].strip().split(" ");
		given_val = [];
		for j in range(0,len(line)):
			if(line[j]!="?"):
				index = id_map[labels[j]];
				given_val += [index];
				var_array[index].assign = line[j];
		(predict,prob) = gibbsSampleMarkov(var_array,poten_array,given_val); 
		

		check = 1;
		check_array = [];
		for k in range(0,len(actual_val)):
			ind_pred = id_map[labels[k]];
			if(ind_pred in given_val):
				continue;
			check_array += [predict[ind_pred]];
			total_elem += 1;
			if(predict[ind_pred]==actual_val[k]):
				elem+=1;
			else:
				# print predict[ind_pred]
				# print actual_val[k]
				check = 0;
		if(check == 1):
			full+=1;
		# print actual_val;
		# print check_array;
		# print "------"
	print "accuracies"
	print total_elem
	print elem/float(total_elem);
	print full/float(len(data))

start = time.time();
poten_array = learn_markov("A3-data/A3-data/andes.dat",var_array,poten_array);
predict_markov("A3-data/andes_test.dat","A3-data/andes_TrueValues.dat",var_array,poten_array,id_map);

# poten_array = learn_markov("A3-data/A3-data/insurance.dat",var_array,poten_array);
# predict_markov("A3-data/insurance_test.dat","A3-data/insurance_TrueValues.dat",var_array,poten_array,id_map);

# poten_array = learn_markov("A3-data/A3-data/hepar2.dat",var_array,poten_array);
# predict_markov("A3-data/hepar_test.dat","A3-data/hepar_TrueValues.dat",var_array,poten_array,id_map);
# for i in range(0,len(var_array)):
# 	poten_array[i].print_node();
print time.time()-start;

#################################################################################################################################################

# loglikelihood -- gibbs sampling and all
# save the potentials read baysian and markov both 
# save the markov network
# report the accuracies
# till 7 PM -- leave for data for report
# complete report and read pgm major notes
# btp report till 1:30 then 30 mins ppt

