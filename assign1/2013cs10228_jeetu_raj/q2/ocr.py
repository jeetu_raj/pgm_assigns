import math;
from pprint import pprint;
from multiprocessing import Pool
import copy
import time

num_char = 10;
num_img_ids = 1000;

poten_si_0 = [[]];		#10*1000
poten_si_t = [[]];		#10*10

poten_si_0sum = [];		#10*1
poten_si_tsum = [];		#10*1

char_map   = {'e':0,'t':1,'a':2,'o':3,'i':4,'n':5,'s':6,'h':7,'r':8,'d':9};
chars	   = ['e','t','a','o','i','n','s','h','r','d'];

all_comb_mem = {};

proc_img_list = [];

skip_const = 5;

########################################################################
# part a
########################################################################


# reads the potential si_0 from given data file
# all factors must be kept in log space
def readSi0(file_name):
	global poten_si_0;
	global num_char;
	global num_img_ids;

	poten_si_0 = [];
	for i in range(0,num_char):
		temp = [];
		for j in range(0,num_img_ids):
			temp+=[0];
		poten_si_0+=[temp];


	file = open(file_name);
	ocr_data = file.read();
	file.close();
	ocr_list = ocr_data.strip().split("\n");
	for elem in ocr_list:
		elem_list = elem.strip().split("\t");
		char_ind  = char_map[elem_list[1]];
		img_id    = int(elem_list[0]);
		val 	  = math.log(float(elem_list[2]));
		poten_si_0[char_ind][img_id] = val;
		# poten_si_0sum[char_ind]+= float(elem_list[2]);


	
# reads the potential si_t from given data file
# all factors must be kept in log space
def readSit(file_name):
	global poten_si_t;
	global num_char;
	global num_img_ids;

	poten_si_t = [];
	for i in range(0,num_char):
		temp = [];
		for j in range(0,num_char):
			temp+=[0];
		poten_si_t+=[temp];


	file = open(file_name);
	trans_data = file.read();
	file.close();
	trans_list = trans_data.strip().split("\n");
	for elem in trans_list:
		elem_list  = elem.strip().split("\t");
		char_ind0  = char_map[elem_list[0]];
		char_ind1  = char_map[elem_list[1]];
		val 	   = math.log(float(elem_list[2]));
		poten_si_t[char_ind0][char_ind1] = val;
		# poten_si_tsum[char_ind0]+= float(elem_list[2]);


# calculates the score for the given observed variables and observed words 
# when only si_o edges present
# returns in logarithm format
def calculateScoreOCR(observed,word):
	global poten_si_0;
	global poten_si_t;
	score = 0;
	for ind in range(0,len(word)):
		score+=poten_si_0[char_map[word[ind]]][observed[ind]];
	return score;


# calculates the score for the given observed variables and observed words 
# when only si_o and si_t edges present
# returns in logarithm format
def calculateScoreTrans(observed,word):
	global poten_si_0;
	global poten_si_t;
	score = 0;
	for ind in range(0,len(word)):
		score+=poten_si_0[char_map[word[ind]]][observed[ind]];
		if(ind < len(word)-1):
			score+=poten_si_t[char_map[word[ind]]][char_map[word[ind+1]]];

	return score;


# calculates the score for the given observed variables and observed words 
# when only si_o edges present
# returns in logarithm format
def calculateScoreComb(observed,word):
	global skip_const
	global poten_si_0;
	global poten_si_t;
	score = 0;
	for ind in range(0,len(word)):
		score+=poten_si_0[char_map[word[ind]]][observed[ind]];
		if(ind < len(word)-1):
			score+=poten_si_t[char_map[word[ind]]][char_map[word[ind+1]]];
		for jind in range(ind+1,len(observed)):
			if(observed[ind]==observed[jind] and word[ind]==word[jind]):
				score+=math.log(skip_const);

	return score;



########################################################################
#part b
########################################################################


def calculateZCombHelp(size):
	global all_comb_mem;
	try:
		return all_comb_mem[size]
	except:
		# print "not found "+str(size);
		if size == 1:
			return chars;
		curr = [];
		for c in chars:
			possible = calculateZCombHelp(size-1);
			for each in possible:
				curr+=[each+c];
		all_comb_mem[size] = curr;
		return curr;

# calculates the Z value for the given observed variables and all 
# possible assignments when only si_o edges present
def calculateZOCR(observed):
	final_val = 1;
	for indo in range(0,len(observed)):
		temp = 0;
		for jind in range(0,num_char):
			temp += math.exp(poten_si_0[jind][observed[indo]]);
		final_val*=temp;
	return final_val

# calculates the Z value for the given observed variables and all 
# possible assignments when only si_o and si_t edges present
def calculateZTrans(observed):
	val_curr = [];
	val_prev = [];
	final_val = 0;
	for i in range(0,num_char):
		val_prev+=[0];
		val_curr+=[0];

	for indo in list(reversed(range(0,len(observed)))):
		if(indo==0):
				for jind in range(0,num_char):
					final_val += math.exp(poten_si_0[jind][observed[indo]]+val_prev[jind]);
		else:
			for ind in range(0,num_char):
				temp = 0;
				for jind in range(0,num_char):
					temp += math.exp(poten_si_0[jind][observed[indo]]+poten_si_t[ind][jind]+val_prev[jind]);
				val_curr[ind] = math.log(temp);

			for i in range(0,num_char):
				val_prev[i] = val_curr[i];
	return final_val

# calculates the Z value for the given observed variables and all 
# possible assignments when all edges are present
def calculateZComb(observed):
	final_val = 0;
	possible = calculateZCombHelp(len(observed));
	for elem in possible:
		final_val += math.exp(calculateScoreComb(observed,elem));
	return final_val



def calculateProbOCR(observed,word):
	Z	  = calculateZOCR(observed);
	score = math.exp(calculateScoreOCR(observed,word));
	return score/Z;

def calculateProbTrans(observed,word):
	Z	  = calculateZTrans(observed);
	score = math.exp(calculateScoreTrans(observed,word));
	return score/Z;

def calculateProbComb(observed,word):
	Z	  = calculateZComb(observed);
	score = math.exp(calculateScoreComb(observed,word));
	return score/Z;


########################################################################
#part c
########################################################################



#########################################################################

def calc_part_help(observed, h , t):
	global poten_si_t;
	val_curr = [];
	val_prev = [];
	word     = "";

	head     = [0,0,0,0,0,0,0,0,0,0];
	val_curr_max = [];
	val_prev_max = [];
	char_curr_max	 = [];
	char_prev_max	 = [];
	final_val = 0;
	max_score = 0;


	for i in range(0,num_char):
		val_prev+=[0];
		val_curr+=[0];
		val_prev_max+=[0];
		val_curr_max+=[0];
		char_prev_max+=[""];
		char_curr_max+=[""];

	if(h>=0):
		for i in range(0,10):
			head[i] = poten_si_t[h][i];
	if(t>=0):
		for i in range(0,10):
			val_prev[i] = poten_si_t[i][t];
			val_prev_max[i] = poten_si_t[i][t];


	# pprint(poten_si_t[0][0])
	# print h;
	# print t;
	# print observed
	# pprint(head);
	# pprint(val_prev_max);



	for indo in list(reversed(range(0,len(observed)))):
		if(indo==0):
			temp_char = "";
			for jind in range(0,num_char):
				final_val += math.exp( head[jind]+poten_si_0[jind][observed[indo]]+val_prev[jind]);
				curr_max = math.exp( head[jind]+poten_si_0[jind][observed[indo]]+val_prev_max[jind]);
				if(curr_max>max_score):
					max_score = curr_max;
					temp_char = chars[jind]+char_prev_max[jind];
			word = temp_char;

		else:
			for ind in range(0,num_char):
				temp = 0;
				temp_max = 0;
				temp_char = "";
				for jind in range(0,num_char):
					curr = math.exp(poten_si_0[jind][observed[indo]]+poten_si_t[ind][jind]+val_prev[jind]);
					curr_max = math.exp(poten_si_0[jind][observed[indo]]+poten_si_t[ind][jind]+val_prev_max[jind]);
					temp+=curr;
					if(curr_max>temp_max):
						temp_max = curr_max;
						temp_char = chars[jind]+char_prev_max[jind];

				val_curr[ind] = math.log(temp);
				val_curr_max[ind] = math.log(temp_max);
				char_curr_max[ind] = temp_char
			word = temp_char+word;

			for i in range(0,num_char):
				val_prev[i] = val_curr[i];
				val_prev_max[i] = val_curr_max[i];
				char_prev_max[i] = char_curr_max[i];


	# print word;
	# print max_score;
	# exit(0);

	return (word,max_score,final_val);


def calc_part(tup, observed):
	fun_max = [];#[[],[],[],[],[],[],[],[],[],[]];
	fun_word_max = []#[[],[],[],[],[],[],[],[],[],[]];
	fun_tot = []#[[],[],[],[],[],[],[],[],[],[]];

	arr = [];
	for i in range(tup[0]+1,tup[1]):
		arr+=[observed[i]];

	# print tup;
	# print arr;


	if(tup[0]<0):
		for i in range(0,10):
			ret = calc_part_help(arr,-1,i);
			fun_max+=[ret[1]]
			fun_word_max+=[ret[0]]
			fun_tot+=[ret[2]]
		return (fun_tot,fun_max,fun_word_max);


	if(tup[1]==len(observed)):
		for i in range(0,10):
			ret = calc_part_help(arr,i,-1);
			fun_max+=[ret[1]]
			fun_word_max+=[ret[0]]
			fun_tot+=[ret[2]]
		return (fun_tot,fun_max,fun_word_max);


	for i in range(0,10):
		fun_max += [[]];
		fun_word_max += [[]];
		fun_tot += [[]];
		for j in range(0,10):
			ret = calc_part_help(arr,i,j);
			fun_max[i]+=[ret[1]]
			fun_word_max[i]+=[ret[0]]
			fun_tot[i]+=[ret[2]]
	return (fun_tot,fun_max,fun_word_max);


def calculatePredCombOptim(observed):
	global skip_const;
	global poten_si_0;
	global poten_si_t;
	skip_mark = [0]*len(observed);
	skip_elems = [];
	dict_count  = {};
	for i in range(0,len(observed)):
		try:
			dict_count[observed[i]] += 1;
		except:
			dict_count[observed[i]] = 1; 

	for i in range(0,len(observed)):
		if(dict_count[observed[i]]>1):
			skip_mark[i] = 1;
			skip_elems += [i];

	# print observed;
	# print skip_mark;
	# print skip_elems;
	# pprint(dict_count);
	#now find the stretches of unskip
	unskip_tup = [];
	i = 0;
	j = 0;
	for ind in range(0,len(skip_mark)):
		if(skip_mark[ind]==0 and ind < len(skip_mark)-1):
			j+=1;
		else:
			if(ind == len(skip_mark)-1 and skip_mark[ind]==0):
				unskip_tup += [(i-1,j+1)];
				continue;
			if(i!=j):
				unskip_tup += [(i-1,j)];
			i = ind+1;
			j = ind+1;

	if(len(unskip_tup)==0):
		return calculatePredComb(observed);

	# print unskip_tup;
	if(len(unskip_tup)==1 and unskip_tup[0][0]==-1 and unskip_tup[0][1]==len(observed)):
		return calculatePredTrans(observed)

	#setup 2d functions for all except last ones
	joined_tabs_Z = {};
	joined_tabs_max = {};
	joined_word_max = {};
	for each in unskip_tup:
		fns = calc_part(each,observed);
		joined_tabs_Z[each] = fns[0];
		joined_tabs_max[each] = fns[1];
		joined_word_max[each] = fns[2];

	# pprint(joined_word_max);
	# pprint(joined_tabs_max);
	# pprint(joined_tabs_Z);
	# exit(0)

	#skip elements - exhaustive way
	possible = calculateZCombHelp(len(skip_elems));

	pos_map  = {};
	for i in range(0,len(skip_elems)):
		pos_map[skip_elems[i]] = i; 


	# print len(possible);

	max_score = 0;
	final_score = 0;
	final_word = "";

	for elem in possible:
		tot_score = 1;
		score = 1;
		frag = [];
		for pair in unskip_tup:
			if(pair[0]==-1 and pair[1]==len(observed)+1):
				print "single word"
				break;
			elif(pair[0]==-1):
				# print elem;
				# pprint(pos_map);
				# print skip_elems
				# print pair
				# print char_map[elem[pos_map[pair[1]]]]
				score*=   joined_tabs_max[pair][ char_map[elem[pos_map[pair[1]]]] ];
				tot_score*= joined_tabs_Z[pair][ char_map[elem[pos_map[pair[1]]]] ];
				frag +=  [joined_word_max[pair][ char_map[elem[pos_map[pair[1]]]] ]]
			elif(pair[1]==len(observed)):
				score*=   joined_tabs_max[pair][ char_map[elem[pos_map[pair[0]]]] ];
				tot_score*= joined_tabs_Z[pair][ char_map[elem[pos_map[pair[0]]]] ];
				frag += [ joined_word_max[pair][ char_map[elem[pos_map[pair[0]]]] ] ];
			else:
				score*=   joined_tabs_max[pair][ char_map[elem[pos_map[pair[0]]]] ][ char_map[elem[pos_map[pair[1]]]] ];
				tot_score*= joined_tabs_Z[pair][ char_map[elem[pos_map[pair[0]]]] ][ char_map[elem[pos_map[pair[1]]]] ];
				frag += [ joined_word_max[pair][ char_map[elem[pos_map[pair[0]]]] ][ char_map[elem[pos_map[pair[1]]]] ] ];
		
		# print frag;
		# print elem
		#transition and ocr edges of all skip
		for ind in range(0,len(skip_elems)):
			score*= math.exp(poten_si_0[ char_map[elem[ind]] ][ observed[ skip_elems[ind]] ])
			tot_score*= math.exp(poten_si_0[ char_map[elem[ind]] ][ observed[ skip_elems[ind]] ])
			if(skip_elems[ind]<len(observed)-1):
				if(skip_mark[skip_elems[ind]+1]==1):
					score*= math.exp(poten_si_t[ char_map[elem[ind]] ][ char_map[elem[ind+1]] ])
					tot_score*= math.exp(poten_si_t[ char_map[elem[ind]] ][ char_map[elem[ind+1]] ])
				
			for jind in range(ind+1,len(skip_elems)):
				if( observed[skip_elems[ind]] == observed[skip_elems[jind]] and elem[ind]==elem[jind]):
					score*=skip_const;
					tot_score*=skip_const;
		# print score;
		# print tot_score
		# exit(0);

		if(score>max_score):
			max_score = score;
			final_word = "";
			array = [""]*len(observed);
			for j in skip_elems:
				array[j] = elem[pos_map[j]];
			for tup in range(0,len(unskip_tup)):
				for k in range(0,len(frag[tup])):
					array[unskip_tup[tup][0]+k+1] = frag[tup][k];
			for p in range(0,len(array)):
				final_word+=array[p];

		# print final_word;
		# print elem;
		# exit(0)
		final_score += tot_score;

	return (final_word,max_score/final_score)


#########################################################################



# perfroms the prediction for the given observed variables 
# when only si_o edges present
def calculatePredOCR(observed):
	final_val = 1;
	max_score = 1;
	word      = "";
	for indo in range(0,len(observed)):
		temp = 0;
		temp_max = 0;
		temp_char= "";
		for jind in range(0,num_char):
			curr = math.exp(poten_si_0[jind][observed[indo]]);
			temp+=curr;
			if(curr>temp_max):
				temp_max = curr;
				temp_char = chars[jind];

		final_val*=temp;
		max_score*=temp_max;
		word+=temp_char;
	return (word,max_score/final_val);

def calculatePredOCRPool(index):
	observed = proc_img_list[index]
	final_val = 1;
	max_score = 1;
	word      = "";
	for indo in range(0,len(observed)):
		temp = 0;
		temp_max = 0;
		temp_char= "";
		for jind in range(0,num_char):
			curr = math.exp(poten_si_0[jind][observed[indo]]);
			temp+=curr;
			if(curr>temp_max):
				temp_max = curr;
				temp_char = chars[jind];

		final_val*=temp;
		max_score*=temp_max;
		word+=temp_char;
	return word;


# perfroms the prediction for the given observed variables  
# when only si_o and si_t edges present
def calculatePredTrans(observed):
	val_curr = [];
	val_prev = [];
	word     = "";
	val_curr_max = [];
	val_prev_max = [];
	char_curr_max	 = [];
	char_prev_max	 = [];
	final_val = 0;
	max_score = 0;
	for i in range(0,num_char):
		val_prev+=[0];
		val_curr+=[0];
		val_prev_max+=[0];
		val_curr_max+=[0];
		char_prev_max+=[""];
		char_curr_max+=[""];

	for indo in list(reversed(range(0,len(observed)))):
		if(indo==0):
			temp_char = "";
			for jind in range(0,num_char):
				final_val += math.exp(poten_si_0[jind][observed[indo]]+val_prev[jind]);
				curr_max = math.exp(poten_si_0[jind][observed[indo]]+val_prev_max[jind]);
				if(curr_max>max_score):
					max_score = curr_max;
					temp_char = chars[jind]+char_prev_max[jind];
			word = temp_char;

		else:
			for ind in range(0,num_char):
				temp = 0;
				temp_max = 0;
				temp_char = "";
				for jind in range(0,num_char):
					curr = math.exp(poten_si_0[jind][observed[indo]]+poten_si_t[ind][jind]+val_prev[jind]);
					curr_max = math.exp(poten_si_0[jind][observed[indo]]+poten_si_t[ind][jind]+val_prev_max[jind]);
					temp+=curr;
					if(curr_max>temp_max):
						temp_max = curr_max;
						temp_char = chars[jind]+char_prev_max[jind];

				val_curr[ind] = math.log(temp);
				val_curr_max[ind] = math.log(temp_max);
				char_curr_max[ind] = temp_char
			word = temp_char+word;

			for i in range(0,num_char):
				val_prev[i] = val_curr[i];
				val_prev_max[i] = val_curr_max[i];
				char_prev_max[i] = char_curr_max[i];

	return (word,max_score/final_val);
	# print final_val;
	# final_val2 = 0;
	# max_curr2 = 0;
	# final_word2 = "";
	# possible = calculateZCombHelp(observed,len(observed));
	# for elem in possible:
	# 	curr = math.exp(calculateScoreTrans(observed,elem));
	# 	if(curr>max_curr2):
	# 		max_curr2 = curr;
	# 		final_word2 = elem;
	# 	final_val2+=curr;
	# print final_val2;
	# return final_word2;

def calculatePredTransPool(index):
	observed = proc_img_list[index]
	val_curr = [];
	val_prev = [];
	word     = "";
	val_curr_max = [];
	val_prev_max = [];
	char_curr_max	 = [];
	char_prev_max	 = [];
	final_val = 0;
	max_score = 0;
	for i in range(0,num_char):
		val_prev+=[0];
		val_curr+=[0];
		val_prev_max+=[0];
		val_curr_max+=[0];
		char_prev_max+=[""];
		char_curr_max+=[""];

	for indo in list(reversed(range(0,len(observed)))):
		if(indo==0):
			temp_char = "";
			for jind in range(0,num_char):
				final_val += math.exp(poten_si_0[jind][observed[indo]]+val_prev[jind]);
				curr_max = math.exp(poten_si_0[jind][observed[indo]]+val_prev_max[jind]);
				if(curr_max>max_score):
					max_score = curr_max;
					temp_char = chars[jind]+char_prev_max[jind];
			word = temp_char;

		else:
			for ind in range(0,num_char):
				temp = 0;
				temp_max = 0;
				temp_char = "";
				for jind in range(0,num_char):
					curr = math.exp(poten_si_0[jind][observed[indo]]+poten_si_t[ind][jind]+val_prev[jind]);
					curr_max = math.exp(poten_si_0[jind][observed[indo]]+poten_si_t[ind][jind]+val_prev_max[jind]);
					temp+=curr;
					if(curr_max>temp_max):
						temp_max = curr_max;
						temp_char = chars[jind]+char_prev_max[jind];

				val_curr[ind] = math.log(temp);
				val_curr_max[ind] = math.log(temp_max);
				char_curr_max[ind] = temp_char
			word = temp_char+word;

			for i in range(0,num_char):
				val_prev[i] = val_curr[i];
				val_prev_max[i] = val_curr_max[i];
				char_prev_max[i] = char_curr_max[i];

	return word;


# performs the prediction for the given observed variables 
# when only si_o edges present
def calculatePredComb(observed):
	final_val = 0;
	max_curr = 0;
	final_word = "";
	possible = calculateZCombHelp(len(observed));
	# pprint(possible)
	for elem in possible:
		curr = math.exp(calculateScoreComb(observed,elem));
		if(curr>max_curr):
			max_curr = curr;
			final_word = elem;
		final_val+=curr;
	return (final_word,max_curr/final_val)

def calculatePredCombPool(index):
	observed = proc_img_list[index];
	final_val = 0;
	max_curr = 0;
	final_word = "";
	possible = calculateZCombHelp(len(observed));
	for elem in possible:
		curr = math.exp(calculateScoreComb(observed,elem));
		if(curr>max_curr):
			max_curr = curr;
			final_word = elem;
		final_val+=curr;
	return final_word



def predict_words(image_file,word_file):
	file_img = open(image_file);
	data_img = file_img.read();
	file_img.close();
	file_word = open(word_file);
	data_word = file_word.read();
	file_word.close();
	img_list = data_img.strip().split("\n");
	word_list = data_word.strip().split("\n");

	total_char   = 0;
	correct_char = 0;
	total_word   = 0;
	correct_word = 0;
	loglikelihood= 0;

	for id in range(0,len(img_list)):
		observed = map(int,img_list[id].strip().split("\t"));
		# print observed

		(predict_word,likelihood) = calculatePredOCR(observed);
		# print predict_word
		# (predict_word,likelihood) = calculatePredTrans(observed)
		#### (predict_word2,likelihood2) = calculatePredComb(observed);
		#(predict_word,likelihood) = calculatePredCombOptim(observed);
		
		# print predict_word

		loglikelihood+= math.log(likelihood)
		
		total_word += 1;
		if(predict_word==word_list[id]):
			correct_word += 1;
		for ind in range(0,len(predict_word)):
			total_char+=1;
			if(word_list[id][ind]==predict_word[ind]):
				correct_char+=1;

		# if(predict_word1!=word_list[id] and predict_word2==word_list[id]):
		# 	print "okk "+predict_word1+" "+predict_word2+" "+word_list[id];

		# if(predict_word1!=word_list[id] and predict_word2!=word_list[id] and predict_word==word_list[id]):
		# 	print "o "+predict_word1+" "+predict_word2+" "+predict_word+" "+word_list[id];

	print "word wise accuracy : "+str(float(correct_word)/float(total_word));
	print "character wise accuracy : "+str(float(correct_char)/float(total_char));
	print "loglikelihood : "+str(loglikelihood);
	print "Average loglikelihood : "+str(loglikelihood/len(word_list));

def multi_process_predict_Words(image_file,word_file,pool):
	global proc_img_list
	file_img = open(image_file);
	data_img = file_img.read();
	file_img.close();
	file_word = open(word_file);
	data_word = file_word.read();
	file_word.close();

	img_list = data_img.strip().split("\n");
	word_list = data_word.strip().split("\n");
	
	for ind in range(0,len(img_list)):
		proc_img_list += [map(int,img_list[ind].strip().split("\t"))];


	total_char   = 0;
	correct_char = 0;
	total_word   = 0;
	correct_word = 0;
	loglikelihood= 0;
	# pprint(proc_img_list)
	# exit(0)
	#predict_word_list = pool.map(calculatePredOCRPool,range(len(proc_img_list)));
	# predict_word_list = pool.map(calculatePredTransPool,range(len(proc_img_list)));
	#predict_word_list = pool.map(calculatePredCombPool,range(len(proc_img_list)));

	for ind in range(0,len(predict_word_list)):
		# loglikelihood+=math.log(calculateProbOCR(proc_img_list[ind],word_list[ind]));
		# loglikelihood+=math.log(calculateProbTrans(proc_img_list[ind],word_list[ind]));
		loglikelihood+=math.log(calculateProbComb(proc_img_list[ind],word_list[ind]));

		# print predict_word+" "+word_list[id]+" "+str(predict_word==word_list[id])
		
		total_word += 1;
		if(predict_word_list[ind]==word_list[ind]):
			correct_word += 1;
		for jind in range(0,len(predict_word_list[ind])):
			total_char+=1;
			if(word_list[ind][jind]==predict_word_list[ind][jind]):
				correct_char+=1;

	print "word wise accuracy : "+str(float(correct_word)/float(total_word));
	print "character wise accuracy : "+str(float(correct_char)/float(total_char));
	print "loglikelihood : "+str(loglikelihood);
	print "Average loglikelihood : "+str(loglikelihood/len(word_list));




#########################################################################
# bonus section : try different changes to see for any improvements
#########################################################################

def change_1(img_file,word_file):
	global poten_si_t;
	global poten_si_0;
	temp_orig = copy.deepcopy(poten_si_t)
	temp_orig_0 = copy.deepcopy(poten_si_0)
	for c in [1,4,5,0.5,0.4]:
		print "#######     "+str(c)+"     ##########";
		poten_si_t = map(lambda x: map( lambda x: x+math.log(c),x), temp_orig);
		predict_words(img_file,word_file);

def change_2(img_file,word_file):
	global poten_si_t;
	global poten_si_0;
	temp_orig = copy.deepcopy(poten_si_t)
	temp_orig_0 = copy.deepcopy(poten_si_0)
	for c in [1,2,4,5,1,0.5,0.4,0.2]:
		print "#######     "+str(c)+"     ##########";
		poten_si_0 = map(lambda x: map( lambda x: x*c,x), temp_orig_0);
		# poten_si_t = map(lambda x: map( lambda x: x*c,x), temp_orig);
		predict_words(img_file,word_file);

def change_3(img_file,word_file):
	global poten_si_t;
	global poten_si_0;
	global skip_const
	temp_orig = copy.deepcopy(poten_si_t)
	temp_orig_0 = copy.deepcopy(poten_si_0)
	for c in [1,2,3,4,5,6,7,8,20,30]:
		print "#######     "+str(c)+"     ##########";
		skip_const = c;
		predict_words(img_file,word_file);



#########################################################################

def initPool(image_file):
	readSi0("OCRdataset/potentials/ocr.dat");
	readSit("OCRdataset/potentials/trans.dat");
	global proc_img_list
	file_img = open(image_file);
	data_img = file_img.read();
	file_img.close();
	img_list = data_img.strip().split("\n");
	
	for ind in range(0,len(img_list)):
		proc_img_list += [map(int,img_list[ind].strip().split("\t"))];	


if __name__ == '__main__':
	# pool = Pool(3, initializer=initPool, initargs=("OCRdataset/data/small/images.dat",));
	# pool = Pool(3, initializer=initPool, initargs=("OCRdataset/data/large/allimages5.dat",));
	# initPool("OCRdataset/data/small/images.dat")
	all_comb_mem[0] = [];
	readSi0("OCRdataset/potentials/ocr.dat");#
	# print len(poten_si_0);
	# print len(poten_si_0[0]);
	# pprint(poten_si_0)
	readSit("OCRdataset/potentials/trans.dat");
	# print len(poten_si_t);
	# print len(poten_si_t[0]);
	# pprint(poten_si_t)
	start =  time.time();

	# print calculatePredOCR([582,969,582,969])
	# print calculateScoreOCR([582,969,582,969],"adad");
	# print calculateScoreTrans([582,969,582,969],"adad");
	# print calculateScoreComb([582,969,582,969],"adad");

	# print calculateZOCR([582,969,582,969]);
	# print calculateZTrans([582,969,582,969]);
	# print calculateZComb([582,969,582,969]);

	# print calculateProbOCR([582,969,582,969],"adad");
	# print calculateProbTrans([582,969,582,969],"adad");
	# print calculateProbComb([582,969,582,969],"adad");

	predict_words("OCRdataset/data-loops.dat","OCRdataset/truth-loops.dat")
	# predict_words("OCRdataset/data/small/images.dat","OCRdataset/data/small/words.dat")
	# print "##############################################################################"
	# predict_words("OCRdataset/data/large/allimages1.dat","OCRdataset/data/large/allwords.dat")
	# print "##############################################################################"
	# predict_words("OCRdataset/data/large/allimages2.dat","OCRdataset/data/large/allwords.dat")
	# # print "##############################################################################"
	# predict_words("OCRdataset/data/large/allimages3.dat","OCRdataset/data/large/allwords.dat")
	# # # print "##############################################################################"
	# predict_words("OCRdataset/data/large/allimages4.dat","OCRdataset/data/large/allwords.dat")
	# # # print "##############################################################################"
	# predict_words("OCRdataset/data/large/allimages5.dat","OCRdataset/data/large/allwords.dat")



	# change_1("OCRdataset/data/small/images.dat","OCRdataset/data/small/words.dat");
	# change_2("OCRdataset/data/small/images.dat","OCRdataset/data/small/words.dat");
	# change_3("OCRdataset/data/small/images.dat","OCRdataset/data/small/words.dat");
	print time.time()-start;






	# multi_process_predict_Words("OCRdataset/data/small/images.dat","OCRdataset/data/small/words.dat",pool)
	# print "##############################################################################"
	# multi_process_predict_Words("OCRdataset/data/large/allimages1.dat","OCRdataset/data/large/allwords.dat",pool)
	# print "##############################################################################"
	# multi_process_predict_Words("OCRdataset/data/large/allimages2.dat","OCRdataset/data/large/allwords.dat",pool)
	# print "##############################################################################"
	# multi_process_predict_Words("OCRdataset/data/large/allimages3.dat","OCRdataset/data/large/allwords.dat",pool)
	# print "##############################################################################"
	# multi_process_predict_Words("OCRdataset/data/large/allimages4.dat","OCRdataset/data/large/allwords.dat",pool)
	# print "##############################################################################"
	# multi_process_predict_Words("OCRdataset/data/large/allimages5.dat","OCRdataset/data/large/allwords.dat",pool)
